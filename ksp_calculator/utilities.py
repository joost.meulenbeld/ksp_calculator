import asyncio
from typing import Callable


async def execute_async(func: Callable, *args):
    return await asyncio.get_running_loop().run_in_executor(None, func, *args)


async def getattr_async(obj, name):
    return await execute_async(getattr, obj, name)


async def setattr_async(obj, name, value):
    await execute_async(setattr, obj, name, value)
