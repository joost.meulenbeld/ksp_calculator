import tkinter
from threading import Thread

import krpc
from ksp_calculator import config

from ksp_calculator.control.control_loop import VerticalNestedController, HorizontalPositionController, \
    VerticalMaxThrustController, \
    ControllerSwitcher, VerticalSpeedController, LandingController, LinearSimpleLaunchController
from ksp_calculator.interface.value_updater_app import ValueUpdaterApp
from ksp_calculator.ksp_integration.action_buttons import SaveGameLoader
from ksp_calculator.ksp_integration.flight_computer import FlightComputer

conn = krpc.connect("Connection", address=config.ip)
fc = FlightComputer(conn)
print(f"Connected to vessel {fc.vessel.name}")

# VAB


vertical_nested_controller = VerticalNestedController(fc)
vertical_max_thrust_controller = VerticalMaxThrustController(fc)
vertical_speed_controller = VerticalSpeedController(fc)
landing_controller = LandingController(fc)
horizontal_position_controller = HorizontalPositionController(fc)
# linear_simple_launch_controller = LinearSimpleLaunchController([[0, 5e3, 10e3, 15e3, 30e3, 40e3, 60e3], [90, 90, 60, 50, 45, 15, 15]], fc)
linear_simple_launch_controller = LinearSimpleLaunchController([[0, 40e3], [90, 0]], fc)
vertical_controller = ControllerSwitcher(fc,
                                         {
                                             "Vertical MaxThrust": vertical_max_thrust_controller,
                                             "Vertical Nested": vertical_nested_controller,
                                             "Vertical Speed": vertical_speed_controller,
                                             "Land": landing_controller,
                                         },
                                         "Vertical MaxThrust")

t_vertical = Thread(target=vertical_controller.start)
t_horizontal = Thread(target=horizontal_position_controller.start)

t_vertical.start()
t_horizontal.start()

print("Started controllers")

save_game_loader = SaveGameLoader(fc, config.save_games)


root = tkinter.Tk()
app = ValueUpdaterApp(root,
                      [
                          *vertical_controller.get_settings(),
                          *horizontal_position_controller.get_settings(),
                          *save_game_loader.get_settings(),
                      ])
root.mainloop()
