import tkinter
from threading import Thread

import krpc

from ksp_calculator.control.control_loop import ControllerSwitcher, LinearSimpleLaunchController
from ksp_calculator.interface.value_updater_app import ValueUpdaterApp
from ksp_calculator.ksp_integration.action_buttons import SaveGameLoader
from ksp_calculator.ksp_integration.flight_computer import FlightComputer

conn = krpc.connect("Connection", address="192.168.114.110")
fc = FlightComputer(conn)
print(f"Connected to vessel {fc.vessel.name}")

# VAB


linear_simple_launch_controller = LinearSimpleLaunchController([[0, 40e3], [90, 0]], fc)

save_game_loader = SaveGameLoader(fc, ["launch_aeroaquus"])
vertical_controller = ControllerSwitcher(fc, {"LinearSimpleLaunch": linear_simple_launch_controller},
                                         "LinearSimpleLaunch")

thread_controller = Thread(target=vertical_controller.start)
thread_controller.start()

print("Started controllers")

root = tkinter.Tk()
app = ValueUpdaterApp(root, [*vertical_controller.get_settings(),
                             *save_game_loader.get_settings()])
root.mainloop()
