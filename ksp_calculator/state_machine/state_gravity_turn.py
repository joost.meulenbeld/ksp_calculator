from ksp_calculator.state_machine.state_launch import LaunchEventEnum, LaunchState, LaunchStateEnum
import numpy as np

class GravityTurn(LaunchState):
    def enter(self):
        self.add_stream(self.fc.velocity_ref_flight, "velocity", self.update_velocity)
        resources = self.fc.vessel.resources_in_decouple_stage(7).with_resource("LiquidFuel")
        self.fuel = [9,9,9,9]

        # How to do this in a for loop???
        self.add_stream(resources[0], "amount", lambda event: self.update_fuel_amount(0, event))
        self.add_stream(resources[1], "amount", lambda event: self.update_fuel_amount(1, event))
        self.add_stream(resources[2], "amount", lambda event: self.update_fuel_amount(2, event))
        self.add_stream(resources[3], "amount", lambda event: self.update_fuel_amount(3, event))
        
        self.velocity = (0, 0, 0)

        self.throttle_setting = 1
        self.fc.disengage_autopilot()
        self.fc.switch_sas(True)
        self.fc.set_sas_mode('prograde')

    def update_velocity(self, velocity):
        self.velocity = velocity

    def update_fuel_amount(self, id, amount):
        self.fuel[id] = amount

    def run(self, event: LaunchEventEnum):
        if event == LaunchEventEnum.periodic:
            speed = np.linalg.norm(self.velocity)
            if speed > 300 and self.throttle_setting != 0.65:
                self.throttle_setting = 0.65
                self.fc.set_throttle(self.throttle_setting)

            if np.sum(self.fuel) < 1.0:
                self.fc.activate_next_stage()
                return LaunchStateEnum.second_stage

        return self.state_enum

    @property
    def state_enum(self):
        return LaunchStateEnum.gravity_turn
