import abc
from enum import Enum
from typing import List


class StateEnum(Enum):
    pass


class EventEnum(Enum):
    pass


class State(metaclass=abc.ABCMeta):
    def run(self, event: EventEnum) -> StateEnum:
        pass

    def enter(self):
        pass

    def exit(self):
        pass

    @property
    @abc.abstractmethod
    def state_enum(self) -> StateEnum:
        pass


class StateMachine:
    def __init__(self, states: List[State], initial_state: StateEnum):
        self.states = {state.state_enum: state for state in states}
        self.current_state_enum = initial_state

    def run(self, event: EventEnum):
        new_state_enum = self.current_state.run(event)
        if new_state_enum != self.current_state_enum:
            print(f"Switch state {self.current_state_enum.name} to {new_state_enum.name}")
            self.current_state.exit()
            self.current_state.remove_streams()
            self.states[new_state_enum].enter()
        self.current_state_enum = new_state_enum

    def force_state(self, new_state_enum: StateEnum):
        if new_state_enum != self.current_state_enum:
            print(f"Manually switching state {self.current_state_enum.name} to {new_state_enum.name}")
            self.current_state.exit()
            self.current_state.remove_streams()
            self.states[new_state_enum].enter()
            self.current_state_enum = new_state_enum

    @property
    def current_state(self) -> State:
        return self.states[self.current_state_enum]
