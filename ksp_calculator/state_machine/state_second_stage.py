from ksp_calculator.state_machine.state_launch import LaunchEventEnum, LaunchState, LaunchStateEnum
import numpy as np

class SecondStage(LaunchState):
    def enter(self):
        self.apoapsis_altitude = 0
        self.add_stream(self.fc.vessel.orbit, "apoapsis_altitude", self.update_apoapsis_altitude)

        self.fc.disengage_autopilot()
        self.fc.switch_sas(True)
        self.fc.set_sas_mode('prograde')

        self.fc.set_throttle(1)

    def update_apoapsis_altitude(self, apoapsis_altitude):
        self.apoapsis_altitude = apoapsis_altitude
    
    def run(self, event: LaunchEventEnum):
        if event == LaunchEventEnum.periodic:
            # compensate for some atmospheric losses (10m)
            error = 80010 - self.apoapsis_altitude
            if error < 0.5:
                self.fc.set_throttle(0)
                return LaunchStateEnum.calculating_lko_circularization
            else:
                throttle_setting = error * 0.0015
                self.fc.set_throttle(throttle_setting)
        return self.state_enum

    @property
    def state_enum(self):
        return LaunchStateEnum.second_stage
