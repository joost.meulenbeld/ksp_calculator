from ksp_calculator.state_machine.state_launch import LaunchEventEnum, LaunchState, LaunchStateEnum


class StraightUp(LaunchState):
    def enter(self):
        # Listen to altitude and pitch
        self.add_stream(self.fc.velocity_ref_flight, "mean_altitude", self.update_altitude)
        self.altitude = 0
        self.pitch_sp = 5

        self.fc.engage_autopilot()
        self.fc.set_throttle(1)
        self.fc.set_pitch(90-self.pitch_sp)
        self.fc.set_heading(90)
        self.fc.set_roll(90)

    def update_altitude(self, altitude):
        self.altitude = altitude

    def run(self, event: LaunchEventEnum):
        if event == LaunchEventEnum.periodic:
            if self.altitude > 1e3 and self.pitch_sp == 5:
                self.pitch_sp = 15
                self.fc.set_pitch(90-self.pitch_sp)
            if self.altitude > 3e3:
                return LaunchStateEnum.gravity_turn
        return self.state_enum

    @property
    def state_enum(self):
        return LaunchStateEnum.straight_up
