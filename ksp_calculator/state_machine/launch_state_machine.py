from ksp_calculator.ksp_integration.flight_computer import FlightComputer
from ksp_calculator.state_machine.state_calculating_next_node import CalculateCircularizeKerbin
from ksp_calculator.state_machine.state_calculating_trans_munar_inejction import CalculatingTransLunarInjection
from ksp_calculator.state_machine.state_circularized import CircularizedLKO
from ksp_calculator.state_machine.state_circularizing import LaunchCircularizingLKO
from ksp_calculator.state_machine.state_gravity_turn import GravityTurn
from ksp_calculator.state_machine.state_idle import Idle
from ksp_calculator.state_machine.state_launch import LaunchStateEnum
from ksp_calculator.state_machine.state_launch_on_launchpad import OnLaunchpad
from ksp_calculator.state_machine.state_launch_straight_up import StraightUp
from ksp_calculator.state_machine.state_machine import StateMachine
from ksp_calculator.state_machine.state_second_stage import SecondStage


class LaunchStateMachine(StateMachine):
    def __init__(self, fc: FlightComputer):
        super().__init__(
            [
                Idle(fc),
                OnLaunchpad(fc),
                StraightUp(fc),
                GravityTurn(fc),
                SecondStage(fc),
                CalculateCircularizeKerbin(fc),
                LaunchCircularizingLKO(fc),
                CircularizedLKO(fc),
                CalculatingTransLunarInjection(fc),
            ],
            LaunchStateEnum.idle
        )
