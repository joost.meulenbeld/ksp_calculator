import abc
import math

from ksp_calculator.state_machine.state_launch import LaunchState, LaunchStateEnum, LaunchEventEnum


class CoastUntilNode(LaunchState, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get_next_state(self):
        pass

    def enter(self):
        self.fc.set_throttle(0)
        self.fc.disengage_autopilot()
        self.fc.switch_sas(True)
        self.fc.set_sas_mode('maneuver')
        self.node = self.fc.get_next_node()
        self.estimated_burn_time = calculate_burn_time(self.fc.vessel.available_thrust, self.fc.vessel.specific_impulse,
                                                       self.fc.get_mass(), self.node.delta_v)

    def run(self, event: LaunchEventEnum) -> LaunchStateEnum:
        if event == LaunchEventEnum.periodic and self.node.time_to < self.estimated_burn_time / 2:
            return self.get_next_state()
        return self.state_enum


class CoastUntilLKOCircularization(CoastUntilNode):
    def get_next_state(self):
        return LaunchStateEnum.circularized_lko

    @property
    def state_enum(self) -> LaunchStateEnum:
        return LaunchStateEnum.coast_to_lko_circularization


def calculate_burn_time(max_thrust, specific_impulse, start_mass, delta_v):
    Isp = specific_impulse * 9.82
    m1 = start_mass / math.exp(delta_v / Isp)
    flow_rate = max_thrust / Isp
    burn_time = (start_mass - m1) / flow_rate
    return burn_time
