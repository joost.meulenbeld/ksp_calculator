from ksp_calculator.state_machine.state_launch import LaunchEventEnum, LaunchState, LaunchStateEnum
import numpy as np

class CalculatingTransLunarInjection(LaunchState):
    def enter(self):
        self.start_time = self.fc.conn.space_center.ut
        self.node_time = self.start_time + 20 * 60
        self.node_prograde = 900
        self.fc.vessel.control.add_node(self.node_time)
        self.fc.get_next_node().prograde = self.node_prograde
        self.substate = 'finding_soi'

    def handle_substate(self):
        node = self.fc.get_next_node()
        if self.substate == 'finding_soi':
            if node.orbit.next_orbit != None:
                if node.orbit.time_to_soi_change < node.orbit.time_to_apoapsis:
                    self.min_error = float('inf')
                    self.substate = 'tuning_periapsis'
                    return False
            self.node_time += 10
            node.ut = self.node_time
        elif self.substate == 'tuning_periapsis':
            error = 300000 - node.orbit.next_orbit.periapsis
            if abs(error) > self.min_error:
                self.substate = 'done'
                return True
            self.min_error = abs(error)
            delta_t = error * 0.00001
            self.node_time -= delta_t
            node.ut = self.node_time
            if abs(error) < 1000:
                self.substate = 'tuning_prograde'
        elif self.substate == 'tuning_prograde':
            self.node_prograde -= 1
            node.prograde = self.node_prograde
            self.min_error = float('inf')
            self.substate = 'tuning_periapsis'
        return False

    def run(self, event: LaunchEventEnum):
        if event == LaunchEventEnum.periodic:
            self.handle_substate()
            
        return self.state_enum

    @property
    def state_enum(self):
        return LaunchStateEnum.calculating_trans_lunar_injection
