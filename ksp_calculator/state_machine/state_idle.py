from ksp_calculator.ksp_integration.flight_computer import FlightComputer
from ksp_calculator.state_machine.state_launch import LaunchStateEnum, LaunchEventEnum, LaunchState
from ksp_calculator.state_machine.state_machine import StateEnum, EventEnum


class Idle(LaunchState):
    def __init__(self, fc: FlightComputer):
        super().__init__(fc)

    def run(self, event: EventEnum) -> StateEnum:
        return self.state_enum

    @property
    def state_enum(self) -> StateEnum:
        return LaunchStateEnum.idle
