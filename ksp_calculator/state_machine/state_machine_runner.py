from ksp_calculator.control.control_loop import Controller
from ksp_calculator.interface.value_updater_app import OptionList
from ksp_calculator.ksp_integration.flight_computer import FlightComputer
from ksp_calculator.state_machine.state_launch import LaunchEventEnum, LaunchStateEnum
from ksp_calculator.state_machine.state_machine import StateMachine


class LaunchStateMachineRunner(Controller):
    def __init__(self, fc: FlightComputer, state_machine: StateMachine):
        super().__init__(fc)
        self.state_machine = state_machine
        self.clickable_events = OptionList(
            [enum.name for enum in LaunchEventEnum],
            callbacks=[lambda enum_name: self.on_event(LaunchEventEnum[enum_name])],
        )
        self.clickable_states = OptionList(
            [enum.name for enum in LaunchStateEnum],
            callbacks=[lambda enum_name: self.state_machine.force_state(LaunchStateEnum[enum_name])],
        )

    def step(self):
        self.on_event(LaunchEventEnum.periodic)

    def on_event(self, event_enum: LaunchEventEnum):
        self.state_machine.run(event_enum)

    def get_settings(self):
        return [self.clickable_events, self.clickable_states]
