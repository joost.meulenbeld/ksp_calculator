import abc

from ksp_calculator.control.solver import SecantSolver
from ksp_calculator.ksp_integration.flight_computer import Apsis
from ksp_calculator.state_machine.state_launch import LaunchEventEnum, LaunchState, LaunchStateEnum


class CalculatingNextNode(LaunchState, metaclass=abc.ABCMeta):
    """Set a node at the next apoapsis or periapsis (set by get_burn_apsis), and calculate the delta-v needed to change
    the opposite apsis to the altitude set by get_burn_apsis.
    If the burn is set (as per get_burn_apsis) to happen on the apoapsis, target altitude is for the periapsis,
    and vice versa.
    """

    @abc.abstractmethod
    def get_target_altitude(self):
        pass

    @abc.abstractmethod
    def get_burn_apsis(self) -> Apsis:
        pass

    @abc.abstractmethod
    def get_next_state(self) -> LaunchStateEnum:
        """Get the state enum to switch to after calculting the next node."""
        pass

    def get_time_to_burn_apsis(self) -> float:
        if self.get_burn_apsis() == Apsis.apoapsis:
            return self.fc.get_time_to_apoapsis()
        return self.fc.get_time_to_periapsis()

    def get_apsis_opposite_burn(self) -> Apsis:
        """Get the apsis that is opposite to the apsis where the burn will take place."""
        if self.get_burn_apsis() == Apsis.apoapsis:
            return Apsis.periapsis
        return Apsis.apoapsis

    def get_opposite_altitude(self, node) -> float:
        """Get the altitude of the orbit after maneuver node, at point opposite to the maneuver node."""
        opposite_ut = self.node_ut + node.orbit.period / 2
        opposite_radius = node.orbit.radius_at(opposite_ut)
        return opposite_radius - self.fc.get_orbital_equatorial_radius()

    def enter(self):
        self.fc.set_throttle(0)
        self.fc.disengage_autopilot()
        self.fc.switch_sas(True)
        self.fc.set_sas_mode('prograde')

        # create maneuver node
        self.node_ut = self.fc.conn.space_center.ut + self.get_time_to_burn_apsis()
        self.fc.vessel.control.add_node(self.node_ut)
        self.solver = SecantSolver()

    def run(self, event: LaunchEventEnum):
        if event == LaunchEventEnum.periodic:
            node = self.fc.get_next_node()
            delta_v_setpoint = self.solver.get_estimation_point()

            node.prograde = delta_v_setpoint
            opposite_altitude = self.get_opposite_altitude(node)
            target_altitude_to_gain = self.get_target_altitude() - opposite_altitude
            self.solver.update_estimate(delta_v_setpoint, target_altitude_to_gain)
            if abs(target_altitude_to_gain) < 10:
                return LaunchStateEnum.launch_circularizing_lko
        return self.state_enum


class CalculateCircularizeKerbin(CalculatingNextNode):
    def get_target_altitude(self):
        return self.fc.get_apsis_altitude(self.get_burn_apsis())

    def get_burn_apsis(self) -> Apsis:
        return Apsis.apoapsis

    @property
    def state_enum(self):
        return LaunchStateEnum.calculating_lko_circularization

    def get_next_state(self):
        return LaunchStateEnum.coast_to_lko_circularization
