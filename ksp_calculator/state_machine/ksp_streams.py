from ksp_calculator.ksp_integration.flight_computer import FlightComputer

# shared with the callbacks that run in separate Thread
shared_state = {
    "queue": None
}

def ut_stream_updated(ut):
    shared_state["queue"].sync_q.put('new_frame')
    pass


def register_ksp_streams(fc, queue):
    shared_state["queue"] = queue
    
    ut_stream = fc.conn.add_stream(getattr, fc.conn.space_center, 'ut')
    ut_stream.add_callback(ut_stream_updated)
    ut_stream.start()