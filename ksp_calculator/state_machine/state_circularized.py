from ksp_calculator.state_machine.state_launch import LaunchEventEnum, LaunchState, LaunchStateEnum


class CircularizedLKO(LaunchState):
    def enter(self):
        pass

    def run(self, event: LaunchEventEnum):
        if event == LaunchEventEnum.press_proceed_button:
            print('got proceed button event')
            return LaunchStateEnum.calculating_trans_lunar_injection

        return self.state_enum

    @property
    def state_enum(self):
        return LaunchStateEnum.circularized_lko
