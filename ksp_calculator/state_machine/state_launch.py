import abc
from enum import auto

from ksp_calculator.ksp_integration.flight_computer import FlightComputer
from ksp_calculator.state_machine.state_machine import EventEnum, StateEnum, State


class LaunchEventEnum(EventEnum):
    periodic = auto()
    press_launch_button = auto()
    press_proceed_button = auto()


class LaunchStateEnum(StateEnum):
    idle = auto()
    on_launchpad = auto()
    straight_up = auto()
    gravity_turn = auto()
    second_stage = auto()
    calculating_lko_circularization = auto()
    coast_to_lko_circularization = auto()
    launch_circularizing_lko = auto()
    circularized_lko = auto()
    calculating_trans_lunar_injection = auto()


class LaunchState(State, metaclass=abc.ABCMeta):
    def __init__(self, fc: FlightComputer):
        super().__init__()
        self.fc = fc
        self.streams = []

    def add_stream(self, obj, attribute, callback):
        new_stream = self.fc.conn.add_stream(getattr, obj, attribute)
        new_stream.add_callback(callback)
        new_stream.start()
        self.streams.append(new_stream)

    def remove_streams(self):
        for s in self.streams:
            s.remove()
        self.streams = []
