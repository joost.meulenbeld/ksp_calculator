import janus
import time

from ksp_calculator.control.control_loop import Controller
from ksp_calculator.interface.value_updater_app import OptionList
from ksp_calculator.ksp_integration.flight_computer import FlightComputer
from ksp_calculator.state_machine.state_launch import LaunchEventEnum, LaunchStateEnum
from ksp_calculator.state_machine.state_machine import StateMachine

class AsyncController():
    def __init__(self, fc: FlightComputer):
        self.fc = fc
        self.running = False
        self.prev_update = 0
        self.periodic_rate = 10

    def step(self):
        raise NotImplementedError

    def handle_event(self):
        raise NotImplementedError

    async def start(self, async_q):
        """Call this function to start the controller"""
        self.startup()
        await self.run(async_q)
        self.cleanup()

    def handle_new_frame(self):
        now = time.monotonic()
        if (now - self.prev_update) > ( 1 / self.periodic_rate ):
            self.handle_event('periodic')
            self.prev_update = now

    async def run(self, async_q):
        self.running = True
        while True:
            event = await async_q.get()
            if (event == "new_frame"):
                self.handle_new_frame()
            else:
                self.handle_event(event)
        self.running = False

    def stop(self):
        # put a stop event in the queue?
        pass

    def startup(self):
        pass

    def cleanup(self):
        pass

class AsyncLaunchStateMachineRunner(AsyncController):
    def __init__(self, fc: FlightComputer, state_machine: StateMachine):
        super().__init__(fc)
        self.state_machine = state_machine
        self.clickable_events = OptionList(
            [enum.name for enum in LaunchEventEnum],
            callbacks=[lambda enum_name: self.on_event(LaunchEventEnum[enum_name])],
        )
        self.clickable_states = OptionList(
            [enum.name for enum in LaunchStateEnum],
            callbacks=[lambda enum_name: self.state_machine.force_state(LaunchStateEnum[enum_name])],
        )

    def handle_event(self, event):
        self.on_event(LaunchEventEnum[event])

    def on_event(self, event_enum: LaunchEventEnum):
        self.state_machine.run(event_enum)

    def get_settings(self):
        return [self.clickable_events, self.clickable_states]
