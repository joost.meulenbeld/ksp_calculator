from ksp_calculator.ksp_integration.flight_computer import FlightComputer
from ksp_calculator.state_machine.state_launch import LaunchStateEnum, LaunchEventEnum, LaunchState
from ksp_calculator.state_machine.state_machine import StateEnum, EventEnum


class OnLaunchpad(LaunchState):
    def __init__(self, fc: FlightComputer):
        super().__init__(fc)

    def run(self, event: EventEnum) -> StateEnum:
        if event == LaunchEventEnum.press_launch_button:
            self.fc.engage_autopilot()
            self.fc.set_throttle(1)
            self.fc.set_pitch(90-5)
            self.fc.set_heading(90)
            self.fc.set_roll(90)
            self.fc.activate_next_stage()
            return LaunchStateEnum.straight_up
        return self.state_enum

    @property
    def state_enum(self) -> StateEnum:
        return LaunchStateEnum.on_launchpad
