import abc

from ksp_calculator.state_machine.state_coast_until_node import calculate_burn_time
from ksp_calculator.state_machine.state_launch import LaunchEventEnum, LaunchState, LaunchStateEnum


class Burning(LaunchState, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get_next_state(self) -> LaunchStateEnum:
        pass

    def enter(self):
        self.fc.set_throttle(0)
        self.fc.disengage_autopilot()
        self.fc.switch_sas(True)
        self.fc.set_sas_mode('maneuver')
        self.node = self.fc.get_next_node()

        self.estimated_burn_time = calculate_burn_time(self.fc.vessel.available_thrust, self.fc.vessel.specific_impulse,
                                                       self.fc.get_mass(), self.node.delta_v)
        print(f'Estimated burn time: {self.estimated_burn_time:.2f}s')

    def exit(self):
        self.fc.set_throttle(0)
        self.node.remove()

    def run(self, event: LaunchEventEnum):
        if event == LaunchEventEnum.periodic:
            if self.node.remaining_delta_v < 1:
                return self.get_next_state()
            if self.node.time_to < self.estimated_burn_time / 2:
                throttle = min(1.0, self.node.remaining_delta_v / 20)
                self.fc.set_throttle(throttle)
        return self.state_enum


class LaunchCircularizingLKO(Burning):
    def get_next_state(self) -> LaunchStateEnum:
        return LaunchStateEnum.circularized_lko

    @property
    def state_enum(self):
        return LaunchStateEnum.launch_circularizing_lko
