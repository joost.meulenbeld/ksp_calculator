import asyncio

import krpc

from ksp_calculator import config
from ksp_calculator.ksp_integration.flight_computer import FlightComputer, Apsis
from ksp_calculator.mission.event import LoadSaveGameEvent, MissionEvent
from ksp_calculator.mission.mission_element_constructor import create_orbital_change
from ksp_calculator.mission.mission_elements import OnLaunchpad, StraightUp, Ignition, GravityTurn, SecondStage, \
    DumpStageInAtmosphere, WaitForProceed, CalculatingMunTransition, BurnMoonTransition, TimewarpToNextNode, \
    WarpToNextSOI, LandBySuicideBurn, ExperimentalMunLaunch, ExperimentalMunApproach
from ksp_calculator.mission.mission_executor import MissionExecutor
from ksp_calculator.mission.mission_segment import MissionSegment
from ksp_calculator.utilities import execute_async
from ksp_calculator.websocket_interface.socket_server import WebsocketInterface


def create_mun_mission() -> MissionSegment:
    mission = MissionSegment("Mission", [
        MissionSegment("LaunchToOrbit", [
            OnLaunchpad(),
            Ignition(),
            StraightUp(),
            GravityTurn(),
            SecondStage(),
            create_orbital_change(Apsis.apoapsis, 80_000, "CircularizeAroundKerbin"),
            DumpStageInAtmosphere(),
        ]),
        MissionSegment("TransitionToMunOrbit", [
            WaitForProceed(),
            CalculatingMunTransition(100_000),
            TimewarpToNextNode(),
            BurnMoonTransition(100_000),
            WarpToNextSOI(),
            create_orbital_change(Apsis.periapsis, 100_000, "CircularizeAroundMun"),
        ]),
        MissionSegment("LandOnMun", [
            WaitForProceed(),
            create_orbital_change(Apsis.apoapsis, -200_000 + 1_000, "Deorbit"),
            LandBySuicideBurn(),
        ]),
        MissionSegment("LandingOnMun", [
            ExperimentalMunApproach()
        ]),
        MissionSegment("TakeoffFromMun", [
            WaitForProceed(),
            ExperimentalMunLaunch()
        ])
    ])
    return mission


async def main():
    conn = krpc.connect("Connection", address=config.ip)

    rx_queue = asyncio.Queue()
    tx_queue = asyncio.Queue()
    fc = FlightComputer(conn, tx_queue)

    mission = create_mun_mission()
    mission_executor = MissionExecutor(fc, mission)
    task = asyncio.create_task(mission_executor.run())

    socket_interface = WebsocketInterface(rx_queue, tx_queue, list(config.save_games.keys()))
    asyncio.create_task(socket_interface.start())

    while event := await rx_queue.get():
        if isinstance(event, LoadSaveGameEvent):
            task.cancel()
            await execute_async(fc.load_savegame, event.save_game_name)
            mission = create_mun_mission()
            mission_executor = MissionExecutor(fc, mission)
            print(f'Load save game {event.save_game_name} with elements {config.save_games[event.save_game_name]}')
            mission.set_mission_recursive(config.save_games[event.save_game_name])
            task = asyncio.create_task(mission_executor.run())
        elif isinstance(event, MissionEvent):
            mission_executor.event_queue.put_nowait(event)
        else:
            raise


if __name__ == '__main__':
    asyncio.run(main())
