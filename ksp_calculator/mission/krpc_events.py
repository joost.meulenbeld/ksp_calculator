import asyncio

from ksp_calculator.ksp_integration.flight_computer import FlightComputer

class LambdaEvent:
    def __init__(self, fc: FlightComputer, obj, attribute, trigger_function=None):
        self._loop = asyncio.get_running_loop()
        self.event = asyncio.Event()
        self.trigger_function = trigger_function

        self.krpc_stream = fc.conn.add_stream(getattr, obj, attribute)
        self.krpc_stream.add_callback(self.callback)

    def __del__(self):
        # Remove the stream
        if self.krpc_stream != None:
            self.krpc_stream.remove()

    def callback(self, value):
        if self.trigger_function(value):
            self._loop.call_soon_threadsafe(self.event.set)

    def greater_than(self, value):
        self.trigger_function = lambda x: x > value
        return self

    def less_than(self, value):
        self.trigger_function = lambda x: x < value
        return self

    async def wait(self):
        if self.trigger_function == None:
            raise Exception("LambdaEvent has no trigger function")

        self.krpc_stream.start()
        try:
            await self.event.wait()
        finally:
            # Remove stream
            self.krpc_stream.remove()
            self.krpc_stream = None

class KrpcEvent:
    def __init__(self, fc: FlightComputer, obj, attribute):
        self.fc = fc
        self.call = fc.conn.get_call(getattr, obj, attribute)
        self.event = None

    def greater_than(self, value):
        expr = self.fc.conn.krpc.Expression.greater_than(
            self.fc.conn.krpc.Expression.call(self.call),
            self.fc.conn.krpc.Expression.constant_double(value)
        )
        self.event = self.fc.conn.krpc.add_event(expr)
        return self

    def blocking_wait(self):
        with self.event.condition:
            print("Start blocking wait")
            self.event.wait()
        print("End blocking wait")

    async def wait(self):
        # Cancellation does not work yet
        try:
            await asyncio.get_running_loop().run_in_executor(None, self.blocking_wait)
        except asyncio.CancelledError:
            print("Try notify")
            with self.event.condition:
                print("Notifying")
                self.event.condition.notify_all()
                print("Was notified")
            raise
        finally:
            print("Finally remove event")
            self.event.remove()