from ksp_calculator.mission.event import MissionEvent


class LaunchEvent(MissionEvent):
    pass

class ProceedEvent(MissionEvent):
    pass