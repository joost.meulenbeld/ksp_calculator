import asyncio
import math
from typing import List

import numpy as np

from ksp_calculator.control.control_loop import get_remaining_burn_time
from ksp_calculator.control.pid import PID
from ksp_calculator.control.solver import SecantSolver
from ksp_calculator.ksp_integration.flight_computer import FlightComputer, Apsis
from ksp_calculator.mission.event import MissionEvent
from ksp_calculator.mission.krpc_events import LambdaEvent, KrpcEvent
from ksp_calculator.mission.mission_element import MissionElement
from ksp_calculator.mission.mission_events import LaunchEvent, ProceedEvent
from ksp_calculator.physics.orbital_mechanics import calculate_burn_time
from ksp_calculator.utilities import execute_async, getattr_async, setattr_async

from analysis.inverse_landing_simulation import calculate_mission_profile
from analysis.bodies import Mun

class OnLaunchpad(MissionElement):
    def __init__(self):
        super().__init__()
        self._event = asyncio.Event()

    async def _run(self, fc: FlightComputer):
        self._event.clear()
        print("Waiting for launch command...")
        await self._event.wait()

    @classmethod
    def get_possible_events(cls) -> List[type(MissionEvent)]:
        return [LaunchEvent]

    def handle_event(self, event: MissionEvent):
        if isinstance(event, LaunchEvent):
            self._event.set()


class Ignition(MissionElement):
    async def _run(self, fc: FlightComputer):
        print("Setting autopilot before launch!")
        await execute_async(fc.engage_autopilot)
        await execute_async(fc.set_throttle, 1)
        await execute_async(fc.set_pitch, 90 - 5)
        await execute_async(fc.set_heading, 90)
        await execute_async(fc.set_roll, 90)
        print("Launch!")
        await execute_async(fc.activate_next_stage)

        # await asyncio.sleep(0.5)


class StraightUp(MissionElement):
    async def _run(self, fc: FlightComputer):
        print("Launched!")

        await LambdaEvent(fc, fc.velocity_ref_flight, "mean_altitude", lambda altitude: altitude > 1000).wait()
        print("Above 1000")
        await execute_async(fc.set_pitch, 90 - 15)

        await LambdaEvent(fc, fc.velocity_ref_flight, "mean_altitude", lambda altitude: altitude > 3000).wait()
        print("Above 3000")


class GravityTurn(MissionElement):
    async def _run(self, fc: FlightComputer):
        await execute_async(fc.disengage_autopilot)
        await execute_async(fc.switch_sas, True)
        await execute_async(fc.set_sas_mode, 'prograde')

        self.speed = 0
        self.speed_event = asyncio.Event()
        self.booster_empty_event = asyncio.Event()
        self._loop = asyncio.get_running_loop()
        self.register_stream_callback(fc, fc.velocity_ref_flight, "velocity", self.update_velocity)

        self.fuel = [9, 9, 9, 9]
        resources = fc.vessel.resources_in_decouple_stage(7).with_resource("LiquidFuel")
        for i in range(4):
            self.register_stream_callback(fc, resources[i], "amount",
                                          lambda event, i=i: self.update_fuel_amount(i, event))

        await self.speed_event.wait()
        await execute_async(fc.set_throttle, 0.65)

        await self.booster_empty_event.wait()
        await execute_async(fc.activate_next_stage)
        await execute_async(fc.set_throttle, 1.0)

    def update_velocity(self, velocity):
        speed = np.linalg.norm(velocity)
        if (self.speed < 300 and speed >= 300):
            self._loop.call_soon_threadsafe(self.speed_event.set)

    def update_fuel_amount(self, id, amount):
        self.fuel[id] = amount
        if np.sum(self.fuel) < 1.0:
            self._loop.call_soon_threadsafe(self.booster_empty_event.set)


class SecondStage(MissionElement):
    async def _run(self, fc: FlightComputer):

        await execute_async(fc.disengage_autopilot)
        await execute_async(fc.switch_sas, True)
        await execute_async(fc.set_sas_mode, 'prograde')
        await execute_async(fc.set_throttle, 1.0)

        self._loop = asyncio.get_running_loop()
        self.apoapsis_altitude = 0
        self.register_stream_callback(fc, fc.vessel.orbit, "apoapsis_altitude", self.update_apoapsis_altitude)

        while True:
            error = 80010 - self.apoapsis_altitude
            if error < 0.5:
                await execute_async(fc.set_throttle, 0)
                break
            else:
                throttle_setting = error * 0.0015
                await execute_async(fc.set_throttle, throttle_setting)

    def update_apoapsis_altitude(self, apoapsis_altitude):
        self.apoapsis_altitude = apoapsis_altitude


class BurnNode(MissionElement):
    def __init__(self, target_altitude: float):
        super().__init__()
        self.target_altitude = target_altitude
        self.ut = float('nan')
        self.orbital_period = float('nan')
        self._timer = asyncio.Event()
        self.next_event_time_ut = None
        self._loop = asyncio.get_running_loop()

    @property
    def opposite_ut(self):  # Can be nan if ut and orbital_period are not set yet!
        return self.ut + self.orbital_period / 2

    def update_ut_callback(self, ut):
        if ut >= self.next_event_time_ut:
            self._loop.call_soon_threadsafe(self._timer.set)

    async def _run(self, fc: FlightComputer):
        node = await execute_async(fc.get_next_node)
        self.update_value_from_stream(fc, fc.conn.space_center, "ut", "ut")
        self.update_value_from_stream(fc, fc.vessel.orbit, "period", "orbital_period")

        delta_v = await getattr_async(node, "delta_v")
        estimated_burn_time = calculate_burn_time(
            await getattr_async(fc.vessel, "available_thrust"),
            await getattr_async(fc.vessel, "specific_impulse"),
            await execute_async(fc.get_mass),
            delta_v
        )
        print(f"Estimated burn time: {estimated_burn_time}")

        is_burn_prograde = await getattr_async(node, "prograde") > 0
        print(f"Burn is prograde? {is_burn_prograde}")

        await execute_async(fc.set_sas_mode, 'maneuver')
        burn_time_ut = await getattr_async(node, "ut") - estimated_burn_time / 2
        await KrpcEvent(fc, fc.conn.space_center, "ut").greater_than(burn_time_ut).wait()

        print("Start burning!")
        await execute_async(fc.set_throttle, 1.0)
        # First burn for estimated_burn_time minus 2 seconds.
        equatorial_radius = await execute_async(fc.get_orbital_equatorial_radius)

        # Then burn in a slower pace until the target altitude is reached.
        while True:
            if math.isinf(self.opposite_ut):
                throttle = 1
                await execute_async(fc.set_throttle, throttle)
                continue
            opposite_altitude = await execute_async(fc.vessel.orbit.radius_at, self.opposite_ut) - equatorial_radius
            altitude_to_gain = self.target_altitude - opposite_altitude
            throttle = min(1., max(0.001, 0.000_04 * abs(altitude_to_gain)))
            print(f"Opposite altitude {opposite_altitude:.1f}, Altitude to gain {altitude_to_gain:.1f}, "
                  f"Throttle {throttle:.6f}")
            if not math.isnan(opposite_altitude) and is_burn_prograde == (opposite_altitude > self.target_altitude):
                break
            await execute_async(fc.set_throttle, throttle)
        await execute_async(fc.set_throttle, 0)
        print("Finished burning!")

        await execute_async(node.remove)


class DumpStageInAtmosphere(MissionElement):
    def __init__(self):
        super().__init__()
        self.equatorial_radius = 0

    async def get_opposite_altitude(self, fc: FlightComputer):
        # Calculate actual opposite altitude
        ut = await getattr_async(fc.conn.space_center, "ut")
        orbital_period = await getattr_async(fc.vessel.orbit, "period")
        opposite_ut = ut + orbital_period / 2
        opposite_altitude = await execute_async(fc.vessel.orbit.radius_at, opposite_ut) - self.equatorial_radius
        return opposite_altitude

    async def _run(self, fc: FlightComputer):
        self.equatorial_radius = await execute_async(fc.get_orbital_equatorial_radius)
        await execute_async(fc.switch_sas, True)
        await execute_async(fc.set_sas_mode, "prograde")
        await execute_async(fc.disengage_autopilot)
        await execute_async(fc.set_throttle, 0)

        original_altitude = await self.get_opposite_altitude(fc)
        print(f"Original altitude: {original_altitude}")
        await execute_async(fc.set_sas_mode, "retrograde")
        await asyncio.sleep(20)
        await execute_async(fc.set_throttle, 0.2)
        while True:
            opposite_altitude = await self.get_opposite_altitude(fc)
            if (opposite_altitude < 65_000):
                break
        await execute_async(fc.set_throttle, 0)
        await asyncio.sleep(1)
        await execute_async(fc.activate_next_stage)
        await execute_async(fc.activate_next_stage)
        await asyncio.sleep(3)
        await execute_async(fc.set_sas_mode, "radial")
        await asyncio.sleep(6)
        await execute_async(fc.set_throttle, 1)
        await asyncio.sleep(0.2)
        await execute_async(fc.set_throttle, 0)
        await execute_async(fc.set_sas_mode, "prograde")
        await asyncio.sleep(6)

        while True:
            # Calculate actual opposite altitude
            opposite_altitude = await self.get_opposite_altitude(fc)
            target_altitude_to_gain = original_altitude - opposite_altitude
            if (target_altitude_to_gain < 10):
                break
            throttle = min(0.000_20 * abs(target_altitude_to_gain), 1)
            await execute_async(fc.set_throttle, throttle)
        await execute_async(fc.set_throttle, 0)
        print('Done Dumping')


class WaitForProceed(MissionElement):
    def __init__(self):
        super().__init__()
        self._event = asyncio.Event()

    def handle_event(self, event: MissionEvent):
        if isinstance(event, ProceedEvent):
            self._event.set()

    async def _run(self, fc: FlightComputer):
        pass
        await self._event.wait()
        print("Proceed command received")

    @classmethod
    def get_possible_events(cls) -> List[type(MissionEvent)]:
        return [ProceedEvent]


class LandBySuicideBurn(MissionElement):
    def __init__(self):
        super().__init__()
        self.height = float('nan')
        self.radius = float('nan')
        self.mass = float('nan')
        self.throttle = float('nan')
        self.surface_velocity = [float('nan'), float('nan'), float('nan')]
        self.ut = float('nan')

    async def _run(self, fc: FlightComputer):
        self.update_value_from_stream(fc, fc.orbital_ref_flight, "surface_altitude", "height")
        self.update_value_from_stream(fc, fc.vessel.orbit, "radius", "radius")
        self.update_value_from_stream(fc, fc.vessel, "mass", "mass")
        self.update_value_from_stream(fc, fc.vessel.control, "throttle", "throttle")
        self.update_value_from_stream(fc, fc.velocity_ref_flight, "velocity", "surface_velocity")
        self.update_value_from_stream(fc, fc.conn.space_center, "ut", "ut")

        await execute_async(fc.set_sas_mode, "retrograde")
        await execute_async(fc.set_throttle, 0)
        await setattr_async(fc.vessel.control, "speed_mode", fc.vessel.control.speed_mode.surface)
        await execute_async(fc.disengage_autopilot)
        mu = await getattr_async(fc.vessel.orbit.body, "gravitational_parameter")

        while self.height > 110:
            g = mu / self.radius ** 2
            vertical_thrust_acceleration = fc.vessel.max_thrust / self.mass * \
                                           await execute_async(fc.get_vertical_direction)
            print(self.height, self.surface_velocity[0], g, vertical_thrust_acceleration)
            remaining_burn_time = get_remaining_burn_time(self.height, self.surface_velocity[0], 100, -100 / 3, -g,
                                                          vertical_thrust_acceleration)
            print(remaining_burn_time)
            should_burn = remaining_burn_time > 0
            if float(should_burn) != self.throttle:
                print("Setting throttle")
                await execute_async(fc.set_throttle, float(should_burn))
            await asyncio.sleep(0.1)

        await setattr_async(fc.vessel.control, "gear", True)
        # await execute_async(fc.set_pitch, 90)
        available_thrust = await getattr_async(fc.vessel, "available_thrust")

        vspeed_controller = PID(kp=10)
        while self.throttle > 0:
            vspeed_controller.setpoint = min(-2., -self.height / 3)

            sp_acceleration = vspeed_controller.step(self.surface_velocity[0], self.ut)
            g = mu / self.radius ** 2
            thrust_acceleration = sp_acceleration + g
            thrust = thrust_acceleration * self.mass / await execute_async(fc.get_vertical_direction)
            if available_thrust == 0:
                throttle = 0
            else:
                throttle = np.clip(thrust / available_thrust, 0, 1)
            print(
                f"Height: {self.height:.1f}, Sp_acceleration: {sp_acceleration:.2f}, thrust: {thrust}, throttle: {throttle}")
            await execute_async(fc.set_throttle, throttle)


class CalculatingApsisOrbitalChange(MissionElement):
    def __init__(self, burn_apsis: Apsis, target_altitude=None):
        super().__init__()
        self.burn_apsis = burn_apsis
        self.target_altitude = target_altitude

    async def _run(self, fc: FlightComputer):
        await execute_async(fc.switch_sas, True)
        await execute_async(fc.disengage_autopilot)
        await execute_async(fc.set_throttle, 0)
        print("Start calculating maneuver")
        # create maneuver node
        ut = await getattr_async(fc.conn.space_center, "ut")
        time_to_node = await execute_async(fc.get_time_to_apsis, self.burn_apsis)
        node_ut = ut + time_to_node
        await execute_async(fc.vessel.control.add_node, node_ut)
        node = await execute_async(fc.get_next_node)
        mu = await getattr_async(fc.vessel.orbit.body, "gravitational_parameter")

        equatorial_radius = await execute_async(fc.get_orbital_equatorial_radius)
        apsis_radius = await execute_async(fc.get_apsis_altitude, self.burn_apsis) + equatorial_radius
        target_radius = self.target_altitude + equatorial_radius
        a_target = (apsis_radius + target_radius) / 2
        a_current = await getattr_async(fc.vessel.orbit, "semi_major_axis")
        v_apsis_before_burn = math.sqrt(mu * (2 / apsis_radius - 1 / a_current))
        v_calculated = math.sqrt(mu * (2 / apsis_radius - 1 / a_target))
        delta_v_setpoint = v_calculated - v_apsis_before_burn
        print(f"Delta-v calculated: {delta_v_setpoint:.2f}")
        await setattr_async(node, "prograde", delta_v_setpoint)

        estimated_burn_time = calculate_burn_time(
            await getattr_async(fc.vessel, "available_thrust"),
            await getattr_async(fc.vessel, "specific_impulse"),
            await execute_async(fc.get_mass),
            delta_v_setpoint
        )
        print(f"Estimated burn time: {estimated_burn_time}")


class CalculatingMunTransition(MissionElement):
    def __init__(self, target_periapsis):
        super().__init__()
        self.target_periapsis = target_periapsis

    async def _run(self, fc: FlightComputer):
        print("Calculating")
        await execute_async(fc.switch_sas, True)
        await execute_async(fc.set_sas_mode, "prograde")
        await execute_async(fc.disengage_autopilot)
        await execute_async(fc.set_throttle, 0)

        ut = await getattr_async(fc.conn.space_center, "ut")
        node_ut = ut + 60

        await execute_async(fc.vessel.control.add_node, node_ut)
        node = await execute_async(fc.get_next_node)
        delta_v_setpoint = 900
        await setattr_async(node, "prograde", delta_v_setpoint)

        # Try to get an encounter with Mun
        while True:
            next_orbit = await getattr_async(node.orbit, "next_orbit")
            if next_orbit != None:
                next_body = await getattr_async(node.orbit.next_orbit.body, "name")
                time_to_soi_change = await getattr_async(node.orbit, "time_to_soi_change")
                time_to_apoapsis = await getattr_async(node.orbit, "time_to_apoapsis")
                if next_body == "Mun" and time_to_soi_change < time_to_apoapsis:
                    print("Mun encounter found")
                    print()
                    break
            node_ut += 30
            await setattr_async(node, "ut", node_ut)

        # 1) solve node time to get Mun periapsis at target altitude
        # 2) decrement prograde velocity by small step
        # 3) go to 1, until target altitude cannot be reached

        best_node_ut = 0
        best_delta_v_setpoint = 0
        while True:
            solver = SecantSolver(initial_guess=node_ut + 1)
            optimum_found = False
            while True:
                node_ut = solver.get_estimation_point()
                await setattr_async(node, "ut", node_ut)
                try:
                    periapsis = await getattr_async(node.orbit.next_orbit, "periapsis")
                except:
                    optimum_found = True
                    break
                altitude_to_loose = periapsis - 300000
                if abs(altitude_to_loose) < 1000:
                    best_node_ut = node_ut
                    best_delta_v_setpoint = delta_v_setpoint
                    break
                solver.update_estimate(node_ut, altitude_to_loose)

            if optimum_found:
                break
            delta_v_setpoint -= 1
            await setattr_async(node, "prograde", delta_v_setpoint)

        await setattr_async(node, "ut", best_node_ut)
        await setattr_async(node, "prograde", best_delta_v_setpoint)


class BurnMoonTransition(MissionElement):
    def __init__(self, target_altitude=100_000):
        super().__init__()
        self.target_altitude = target_altitude
        self._start_burn_timer = asyncio.Event()
        self.start_burn_time = None
        self._loop = asyncio.get_running_loop()
        self.mun_periapsis_altitude = 0

    def update_ut_callback(self, ut):
        if ut >= self.start_burn_time:
            self._loop.call_soon_threadsafe(self._start_burn_timer.set)

    def update_mun_periapsis_altitude(self, mun_periapsis_altitude):
        self.mun_periapsis_altitude = mun_periapsis_altitude

    async def _run(self, fc: FlightComputer):
        await execute_async(fc.switch_sas, True)
        await execute_async(fc.set_sas_mode, "maneuver")
        await execute_async(fc.disengage_autopilot)
        await execute_async(fc.set_throttle, 0)

        node = await execute_async(fc.get_next_node)

        estimated_burn_time = calculate_burn_time(
            await getattr_async(fc.vessel, "available_thrust"),
            await getattr_async(fc.vessel, "specific_impulse"),
            await execute_async(fc.get_mass),
            await getattr_async(node, "delta_v")
        )
        print(f"Estimated burn time: {estimated_burn_time}")
        self.start_burn_time = await getattr_async(node, "ut") - estimated_burn_time / 2
        self.register_stream_callback(fc, fc.conn.space_center, "ut", self.update_ut_callback)
        await self._start_burn_timer.wait()
        print("Start burning to the moon!")
        await execute_async(fc.set_throttle, 1.0)

        # Burn until next orbit is around Mun
        while True:
            next_orbit = await getattr_async(fc.vessel.orbit, "next_orbit")
            if next_orbit == None:
                continue
            next_body = await getattr_async(fc.vessel.orbit.next_orbit.body, "name")
            if next_body == "Mun":
                break

        # Register a stream on the Mun periapsis altitude
        self.register_stream_callback(fc, fc.vessel.orbit.next_orbit, "periapsis_altitude",
                                      self.update_mun_periapsis_altitude)
        while True:
            altitude_to_loose = self.mun_periapsis_altitude - self.target_altitude
            if (altitude_to_loose < 10):
                break
            throttle = min(0.000_002 * abs(altitude_to_loose), 1)
            await execute_async(fc.set_throttle, throttle)

        await execute_async(fc.set_throttle, 0)

        node = await execute_async(fc.get_next_node)
        await execute_async(node.remove)


class TimewarpToNextNode(MissionElement):
    async def _run(self, fc: FlightComputer):
        node = await execute_async(fc.get_next_node)

        estimated_burn_time = calculate_burn_time(
            await getattr_async(fc.vessel, "available_thrust"),
            await getattr_async(fc.vessel, "specific_impulse"),
            await execute_async(fc.get_mass),
            await getattr_async(node, "delta_v")
        )
        print(f"Estimated burn time: {estimated_burn_time}")

        stop_warp_ut = await getattr_async(node, "ut") - estimated_burn_time / 2 - 15
        await execute_async(fc.conn.space_center.warp_to, stop_warp_ut)


class WarpToNextSOI(MissionElement):
    async def _run(self, fc: FlightComputer):
        now = await getattr_async(fc.conn.space_center, "ut")
        time_to_soi_change = await getattr_async(fc.vessel.orbit, "time_to_soi_change")
        print(f"Time to next SOI: {time_to_soi_change}")

        stop_warp_ut = now + time_to_soi_change + 5
        await execute_async(fc.conn.space_center.warp_to, stop_warp_ut)


class LandHere(MissionElement):
    def __init__(self):
        super().__init__()
        self.height = None
        self.ut = None
        self.surface_velocity = None
        self.orbital_vector = None
        self.mass = None
        self.available_thrust = None
        self.situation = None
        self.gear_activated = False

    async def _run(self, fc: FlightComputer):
        print("Register streams")
        self.update_value_from_stream(fc, fc.orbital_ref_flight, "surface_altitude", "height")
        self.update_value_from_stream(fc, fc.conn.space_center, "ut", "ut")
        self.update_value_from_stream(fc, fc.velocity_ref_flight, "velocity", "surface_velocity")
        self.update_value_from_stream(fc, fc.velocity_ref_flight, "center_of_mass", "orbital_vector")
        self.update_value_from_stream(fc, fc.vessel, "mass", "mass")
        self.update_value_from_stream(fc, fc.vessel, "available_thrust", "available_thrust")
        self.update_value_from_stream(fc, fc.vessel, "situation", "situation")
        print("Done registering")
        while (self.height is None or self.ut is None or self.surface_velocity is None or self.orbital_vector is None
               or self.mass is None or self.available_thrust is None or self.situation is None):
            await asyncio.sleep(0.1)
        situation_enum = type(self.situation)

        mu = await getattr_async(fc.vessel.orbit.body, "gravitational_parameter")

        vspeed_controller = PID(3, debug=False)
        north_controller = PID(3, debug=False)
        east_controller = PID(3, debug=False)
        await setattr_async(fc.vessel.auto_pilot, "target_heading", 0)
        await setattr_async(fc.vessel.auto_pilot, "target_pitch", 90)
        # await setattr_async(fc.vessel.auto_pilot, "target_roll", 0)
        await execute_async(fc.engage_autopilot)

        while self.situation != situation_enum.landed:
            # Calculate vertical control
            # vspeed_controller.setpoint = 0
            h_speed = math.sqrt(self.surface_velocity[1] ** 2 + self.surface_velocity[2] ** 2)
            if h_speed > 1 and self.height < 10:
                vspeed_controller.setpoint = 0
            else:
                vspeed_controller.setpoint = min(-2, -self.height / 3)

            g = mu / self.orbital_vector[0] ** 2
            sp_acceleration = vspeed_controller.step(self.surface_velocity[0], self.ut)
            required_acceleration = sp_acceleration + g
            required_thrust = required_acceleration * self.mass / await execute_async(fc.get_vertical_direction)
            if self.available_thrust == 0:
                throttle = 0
            else:
                throttle = np.clip(required_thrust / self.available_thrust, 0, 1)
            # Calculate horizontal control
            north_angle = north_controller.step(self.surface_velocity[1], self.ut)
            east_angle = east_controller.step(self.surface_velocity[2], self.ut)

            total_angle = math.sqrt(north_angle ** 2 + east_angle ** 2)
            rescale = max(1., total_angle / 45)
            north_angle /= rescale
            east_angle /= rescale
            total_angle /= rescale

            sp_pitch = 90 - total_angle
            sp_heading = math.degrees(math.atan2(east_angle, north_angle))

            # Set actuators
            await setattr_async(fc.vessel.control, "throttle", throttle)
            await setattr_async(fc.vessel.auto_pilot, "target_heading", sp_heading)
            await setattr_async(fc.vessel.auto_pilot, "target_pitch", sp_pitch)
            if self.height < 17 and not self.gear_activated:
                self.gear_activated = True
                await setattr_async(fc.vessel.control, "gear", True)
        fc.set_throttle(0)


class ExperimentalMunLanding(MissionElement):
    async def _run(self, fc: FlightComputer):
        print("Deorbit")
        await execute_async(fc.activate_next_stage)
        await execute_async(fc.disengage_autopilot)
        await execute_async(fc.switch_sas, True)
        await execute_async(fc.set_sas_mode, "retrograde")
        await asyncio.sleep(5.1)
        await execute_async(fc.set_throttle, 0.9)
        await asyncio.sleep(12.37)
        await execute_async(fc.set_throttle, 0)

        await execute_async(fc.engage_autopilot)
        await execute_async(fc.set_pitch, 20)
        await execute_async(fc.set_heading, -90)
        await execute_async(fc.set_roll, 90)

        print("Timewarp")
        now = await getattr_async(fc.conn.space_center, "ut")
        await execute_async(fc.conn.space_center.warp_to, now + 137.3 - 23.17)

        print("Decelerate")
        await execute_async(fc.set_throttle, 0.9)
        await asyncio.sleep(23.19)
        await execute_async(fc.set_throttle, 0)
        await execute_async(fc.set_pitch, 90)


class ExperimentalMunLaunch(MissionElement):
    async def _run(self, fc: FlightComputer):
        mass = await execute_async(fc.get_mass)
        print("mass is", mass)
        return
        await execute_async(fc.engage_autopilot)
        await execute_async(fc.set_pitch, 90 - 5)
        await execute_async(fc.set_heading, 90)
        await execute_async(fc.set_roll, 90)

        await execute_async(fc.set_throttle, 0.5)
        print("Launch!")
        # await execute_async(fc.activate_next_stage)
        await asyncio.sleep(1)
        await execute_async(fc.set_throttle, 0)
        await execute_async(fc.set_pitch, 5)
        await asyncio.sleep(2)
        await execute_async(fc.set_throttle, 0.9)
        await asyncio.sleep(28)
        await execute_async(fc.set_throttle, 0)

class ExperimentalMunApproach(MissionElement):
    def __init__(self):
        super().__init__()
        self.mun = Mun()
        self.ut = float('nan')
        self.longitude = float('nan')
        self.radius = float('nan')
        self.mass = float('nan')
        self.velocity = None
        self.ap_engaged = False

    async def _run(self, fc: FlightComputer):
        await execute_async(fc.switch_sas, True)
        await execute_async(fc.set_sas_mode, "retrograde")
        
        self.update_value_from_stream(fc, fc.conn.space_center, "ut", "ut")
        self.update_value_from_stream(fc, fc.vessel.orbit, "radius", "radius")
        t, x, u = await execute_async(calculate_mission_profile)

        profile_longitude_rad = np.rad2deg(np.deg2rad(-10) - x[2, :] + t * self.mun.get_angular_velocity())
        profile_message = {
            "message_type": "flight_profile",
            "message": {
                "longitude": profile_longitude_rad.tolist(),
                "radius": (x[0, :] + 5).tolist()
            }
        }
        fc.send_telemetry(profile_message)

        print(t, u)
        print("Waiting for longitude -47")
        await LambdaEvent(fc, fc.vessel.flight(), "longitude", lambda longitude: longitude > -47.0).wait()
        start_ut = self.ut
        print(f"Start UT: {start_ut}")
        start_ut = 42199.0097929821
        self.update_value_from_stream(fc, fc.vessel.flight(), "longitude", "longitude")
        self.update_value_from_stream(fc, fc.velocity_ref_flight, "velocity", "velocity")
        self.update_value_from_stream(fc, fc.vessel, "mass", "mass")

        lateral_error_filtered = 0
        vertical_error_filtered = 0

        while True:
            profile_t = 280.0 - (self.ut - start_ut)
            profile_i = int((profile_t * 10) + 0.5) - 1
            throttle = u[0, profile_i]
            pitch = u[1, profile_i]

            profile_longitude_rad = np.deg2rad(-10) - x[2, profile_i] + profile_t * self.mun.get_angular_velocity()
            lateral_error = (profile_longitude_rad - np.deg2rad(self.longitude)) * self.radius
            previous_lateral_error_filtered = lateral_error_filtered
            lateral_error_filtered = 0.95 * lateral_error_filtered + 0.05 * lateral_error
            lateral_speed_filtered = lateral_error_filtered - previous_lateral_error_filtered
            profile_r = x[0, profile_i] + 5
            
            previous_vertical_error_filtered = vertical_error_filtered
            vertical_error = profile_r - self.radius
            vertical_error_filtered = 0.95 * vertical_error_filtered + 0.05 * vertical_error
            vertical_speed_filtered = vertical_error_filtered - previous_vertical_error_filtered

            additional_pitch = 0.3 * vertical_error_filtered + 20 * vertical_speed_filtered
            additional_pitch = np.clip(additional_pitch, -10, 10)

            additional_throttle = -0.001 * lateral_error_filtered - 0.01 * lateral_speed_filtered
            additional_throttle = np.clip(additional_throttle, -0.1, 0.1)

            # print(f"h: {vertical_error_filtered:.1f} m\tvs: {vertical_speed_filtered:.1f} m/s\tpitch: {(10 + additional_pitch):.1f}")

            if (throttle > 0.7):
                throttle += additional_throttle
            # print(f"x: {lateral_error_filtered:.1f} m\tdx: {lateral_speed_filtered:.1f} m/s\tthrottle: {throttle:.3f}")

            distance_to_target = np.deg2rad(-10 - self.longitude) * self.radius
            required_acceleration  = (1/2) * self.velocity[2]**2 / distance_to_target
            # print(f"distance to target: {distance_to_target:.1f} m\trequired acceleration: {required_acceleration:.1f}\t{60_000 / self.mass * throttle}")

            if (distance_to_target < 1000):
                throttle = required_acceleration * self.mass / 60_000

            await execute_async(fc.set_throttle, throttle)
            await execute_async(fc.set_pitch, 10 + additional_pitch)

            if (profile_t < 240 and not self.ap_engaged):
                print('engage ap')
                await execute_async(fc.switch_sas, False)
                await execute_async(fc.set_pitch, 10)
                await execute_async(fc.engage_autopilot)
                await execute_async(fc.set_pitch, 10)
                await execute_async(fc.set_heading, 270)
                await execute_async(fc.set_roll, 0)
                self.ap_engaged = True
            
            if (distance_to_target < 1.0):
                await execute_async(fc.set_throttle, 0.0)
                await execute_async(fc.set_pitch, 90)
                await execute_async(fc.set_heading, 270)
                await execute_async(fc.set_roll, 0)
                break
                
            fc.send_telemetry({
                "message_type": "vehicle_state",
                "message": {
                    "longitude": self.longitude,
                    "radius": self.radius
                }
            })