class Event:
    @classmethod
    def parse(cls, **kwargs):
        return cls(**kwargs)

    @classmethod
    def name(cls) -> str:
        return cls.__name__


class MissionEvent(Event):
    pass


class LoadSaveGameEvent(Event):
    def __init__(self, save_game_name: str):
        self.save_game_name = save_game_name


def get_event_by_name(name: str) -> type(Event):
    all_events = [*MissionEvent.__subclasses__(), LoadSaveGameEvent]
    event_mapping = {event.name(): event for event in all_events}
    if name not in event_mapping:
        raise ValueError(f"Event {name} is not defined.")
    return event_mapping[name]
