import asyncio
from concurrent.futures.thread import ThreadPoolExecutor

from ksp_calculator.ksp_integration.flight_computer import FlightComputer
from ksp_calculator.mission.mission_segment import MissionSegment


class MissionExecutor:
    def __init__(self, fc: FlightComputer, mission: MissionSegment):
        self.fc = fc
        self.mission = mission
        self.thread_pool_executor = ThreadPoolExecutor()
        self.event_queue = asyncio.Queue()
        asyncio.get_event_loop().set_default_executor(self.thread_pool_executor)

    async def event_listener(self):
        while event := await self.event_queue.get():
            self.mission.get_current_mission_element().handle_event(event)

    async def run(self):
        task = asyncio.create_task(self.event_listener())
        try:
            while not (finished := await self.mission.run(self.fc)):
                print("New state:")
                print(self.mission.get_current_mission_recursive())
        except asyncio.CancelledError:
            print("Caught cancel in executor")
            task.cancel()

        print("Mission execution finished")
