import abc
import asyncio
from typing import List

from ksp_calculator.ksp_integration.flight_computer import FlightComputer
from ksp_calculator.mission.event import MissionEvent


class MissionElement:
    def __init__(self):
        self.streams = list()
        self.event_queue = asyncio.Queue(1)

    async def run(self, fc: FlightComputer) -> bool:
        try:
            result = await self._run(fc)
            finished = result or result is None
        finally:
            self.exit()
        return finished

    def exit(self):
        for s in self.streams:
            s.remove()
        self.streams = list()

    def handle_event(self, event: MissionEvent):
        pass

    def register_stream_callback(self, fc: FlightComputer, obj, attribute, callback):
        new_stream = fc.conn.add_stream(getattr, obj, attribute)
        new_stream.add_callback(callback)
        new_stream.start()
        self.streams.append(new_stream)

    def update_value_from_stream(self, fc: FlightComputer, obj, obj_attribute: str, self_attribute: str):
        self.register_stream_callback(fc, obj, obj_attribute, lambda value: setattr(self, self_attribute, value))

    @abc.abstractmethod
    async def _run(self, fc: FlightComputer):
        pass

    def __str__(self):
        return self.__class__.__name__

    @classmethod
    def get_possible_events(cls) -> List[type(MissionEvent)]:
        return []
