from typing import List

from ksp_calculator.ksp_integration.flight_computer import FlightComputer
from ksp_calculator.mission.mission_element import MissionElement


class MissionSegment(MissionElement):
    def __init__(self, name: str, mission_elements: List[MissionElement]):
        super().__init__()
        self.name = name
        self.mission_elements = mission_elements
        self.current_element_index = 0

    @property
    def current_element(self) -> MissionElement:
        return self.mission_elements[self.current_element_index]

    async def _run(self, fc: FlightComputer):
        child_finished = await self.current_element.run(fc)
        if child_finished:
            self.current_element_index += 1
        self_finished = self.current_element_index == len(self.mission_elements)
        return self_finished

    def get_current_mission_recursive(self) -> List[str]:
        states_indices = [str(self.current_element)]
        if isinstance(self.current_element, MissionSegment):
            states_indices.extend(self.current_element.get_current_mission_recursive())
        return states_indices

    def set_mission_recursive(self, element_list: List[str]):
        next_element_name = element_list[0]

        indices = [i for i, element in enumerate(self.mission_elements) if str(element) == next_element_name]
        assert len(indices) == 1, f"Should have exactly 1 mission element with name {next_element_name}"

        self.current_element_index = indices[0]
        assert len(element_list) == 1 or isinstance(self.current_element, MissionSegment)

        if len(element_list) > 1:
            self.current_element.set_mission_recursive(element_list[1:])

    def get_current_mission_element(self) -> MissionElement:
        if isinstance(self.current_element, MissionSegment):
            return self.current_element.get_current_mission_element()
        elif isinstance(self.current_element, MissionElement):
            return self.current_element
        raise ValueError

    def __str__(self):
        return self.name
