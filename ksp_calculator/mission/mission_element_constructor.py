from ksp_calculator.ksp_integration.flight_computer import Apsis
from ksp_calculator.mission.mission_elements import CalculatingApsisOrbitalChange, TimewarpToNextNode, BurnNode
from ksp_calculator.mission.mission_segment import MissionSegment


def create_orbital_change(apsis: Apsis, target_altitude: float, name: str) -> MissionSegment:
    segment = MissionSegment(name, [
        CalculatingApsisOrbitalChange(apsis, target_altitude),
        TimewarpToNextNode(),
        BurnNode(target_altitude),
    ])
    return segment


def create_circularize(target_altitude: float, name: str, first_apsis: Apsis=Apsis.apoapsis) -> MissionSegment:
    second_apsis = first_apsis.opposite
    segment = MissionSegment(name, [
        create_orbital_change(first_apsis, target_altitude, str(first_apsis)),
        create_orbital_change(second_apsis, target_altitude, str(second_apsis))
    ])
    return segment
