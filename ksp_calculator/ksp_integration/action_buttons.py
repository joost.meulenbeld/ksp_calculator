from typing import List, Callable, Dict

from ksp_calculator.interface.value_updater_app import Displayable, OptionList
from ksp_calculator.ksp_integration.flight_computer import FlightComputer


class SaveGameLoader(Displayable):
    def __init__(self, fc: FlightComputer, save_game_names: Dict[str, str],
                 load_save_game_callback: Callable = lambda save_game_name: 0):
        """
        :param load_savegame_callback: Function taking a save game name and executing any extra logic after loading game
        """
        super().__init__()
        self.fc = fc
        self.save_game_names = save_game_names
        self.load_save_game_callback = load_save_game_callback
        self.save_game_option_list = OptionList(list(self.save_game_names.keys()),
                                                callbacks=[lambda save_game_name: self.start_savegame(save_game_name)])

    def get_settings(self):
        return [self.save_game_option_list]

    def start_savegame(self, save_game_name: str):
        print(f"Starting savegame {save_game_name}")
        self.fc.load_savegame(save_game_name)
        self.load_save_game_callback(save_game_name)
