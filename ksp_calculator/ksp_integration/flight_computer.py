import enum

import numpy as np


class Apsis(enum.Enum):
    apoapsis = enum.auto()
    periapsis = enum.auto()

    @property
    def opposite(self) -> "Apsis":
        if self == Apsis.apoapsis:
            return Apsis.periapsis
        return Apsis.apoapsis


class FlightComputer:
    def __init__(self, conn, telemetry_queue=None):
        self.conn = conn
        self.telemetry_queue = telemetry_queue
        self._load_krpc_objects()

        body_name_stream = conn.add_stream(getattr, self.vessel.orbit.body, "name")
        body_name_stream.add_callback(self._load_reference_frames)
        body_name_stream.start()

    def _load_krpc_objects(self):
        self.vessel = self.conn.space_center.active_vessel
        print(f"Connected to vessel {self.vessel.name}")
        self._load_reference_frames(self.vessel.orbit.body.name)

    def _load_reference_frames(self, name=None):
        print(f"Load reference frames for {name}")
        self.velocity_reference_frame = self.conn.space_center.ReferenceFrame.create_hybrid(
            position=self.vessel.orbit.body.reference_frame,
            rotation=self.vessel.surface_reference_frame)
        self.velocity_ref_flight = self.vessel.flight(reference_frame=self.velocity_reference_frame)
        self.orbital_ref_flight = self.vessel.flight(reference_frame=self.vessel.orbit.body.reference_frame)
        self.non_rotating_ref_flight = self.vessel.flight(
            reference_frame=self.vessel.orbit.body.non_rotating_reference_frame)

    def send_telemetry(self, message):
        if self.telemetry_queue:
            self.telemetry_queue.put_nowait(message)

    def get_surface_velocity(self) -> np.array:
        """Get surface velocity (up velocity, north velocity, east velocity)"""
        return np.array(self.velocity_ref_flight.velocity)

    def get_orbital_velocity(self) -> np.array:
        """Get orbital velocity (up velocity, north velocity, east velocity)"""
        return np.array(self.non_rotating_ref_flight.velocity)

    def get_orbital_speed(self) -> float:
        return self.non_rotating_ref_flight.speed

    def get_ship_orbital_radius(self) -> float:
        """Get distance from currently orbited body to com of ship"""
        return self.velocity_ref_flight.center_of_mass[0]

    def get_orbital_equatorial_radius(self) -> float:
        return self.vessel.orbit.body.equatorial_radius

    def get_altitude(self) -> float:
        return self.velocity_ref_flight.mean_altitude

    def get_height(self) -> float:
        return self.orbital_ref_flight.surface_altitude

    def get_g(self) -> float:
        """Get gravitational acceleration at current location"""
        return self.vessel.orbit.body.gravitational_parameter / self.get_ship_orbital_radius() ** 2

    def get_mass(self) -> float:
        return self.vessel.mass

    def get_vertical_direction(self):
        return self.vessel.direction(self.velocity_reference_frame)[0]

    def get_apsis_altitude(self, apsis: Apsis):
        if apsis == Apsis.apoapsis:
            return self.get_apoapsis_altitude()
        return self.get_periapsis_altitude()

    def get_apoapsis_altitude(self):
        return self.vessel.orbit.apoapsis_altitude

    def get_periapsis_altitude(self):
        return self.vessel.orbit.periapsis_altitude

    def get_time_to_apsis(self, apsis: Apsis):
        if apsis == Apsis.apoapsis:
            return self.get_time_to_apoapsis()
        return self.get_time_to_periapsis()

    def get_time_to_apoapsis(self):
        return self.vessel.orbit.time_to_apoapsis

    def get_time_to_periapsis(self):
        return self.vessel.orbit.time_to_periapsis

    def get_longitude(self):
        return self.vessel.flight().longitude

    def get_angle_of_attack(self):
        return self.vessel.flight().angle_of_attack

    def set_throttle(self, throttle: float):
        self.vessel.control.throttle = throttle

    def set_heading(self, heading: float):
        self.vessel.auto_pilot.target_heading = heading

    def set_roll(self, roll: float):
        self.vessel.auto_pilot.target_roll = roll

    def set_pitch(self, pitch: float):
        self.vessel.auto_pilot.target_pitch = pitch

    def activate_next_stage(self):
        self.vessel.control.activate_next_stage()

    def engage_autopilot(self):
        self.vessel.auto_pilot.engage()

    def disengage_autopilot(self):
        self.vessel.auto_pilot.disengage()

    def switch_sas(self, new_state: bool):
        self.vessel.auto_pilot.sas = new_state

    def set_sas_mode(self, new_state: str):
        sas_mode_enum = self.conn.space_center.SASMode[new_state]
        self.vessel.auto_pilot.sas_mode = sas_mode_enum

    def load_savegame(self, save_game_name: str):
        self.conn.space_center.load(save_game_name)
        self._load_krpc_objects()

    def get_next_node(self):
        return self.vessel.control.nodes[0]
