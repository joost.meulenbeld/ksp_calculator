from collections import UserDict
from typing import NamedTuple


class Coordinate(NamedTuple):
    lat: float
    lon: float


class Location(NamedTuple):
    name: str
    coord: Coordinate


class _LocationManager(UserDict):
    """Dictionary mapping location name to Location tuple. Add elements with add_location."""

    def add_location(self, location: Location):
        assert isinstance(location, Location)
        self[location.name] = location

    def __setitem__(self, key, value):
        assert isinstance(key, str)
        assert isinstance(value, Location)
        assert key == value.name
        super().__setitem__(key, value)


# Singleton location manager
LocationManager = _LocationManager()

LocationManager.add_location(Location("LaunchSite", Coordinate(lat=-0.0971901709300802, lon=-74.55768367523336)))
LocationManager.add_location(Location("VAB", Coordinate(lat=-0.09680212, lon=-74.6167)))
LocationManager.add_location(
    Location("Island airfield", Coordinate(lat=-1 - 32 / 60 - 27 / 60 / 60, lon=-71 - 54 / 60 - 35 / 60 / 60)))
