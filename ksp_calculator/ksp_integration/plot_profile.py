import math

import krpc
import matplotlib.animation as animation
import matplotlib.pyplot as plt

from ksp_calculator.ksp_integration.flight_computer import FlightComputer

conn = krpc.connect("Connection", address="192.168.114.110")
fc = FlightComputer(conn)
print(f"Connected to vessel {fc.vessel.name}")

# Create figure for plotting
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.set_aspect('equal')
xs = []
ys = []
plt.grid('on')

start_longitude = fc.get_longitude()

# This function is called periodically from FuncAnimation
def animate(i, xs, ys):
    # Add x and y to lists
    xs.append(fc.get_orbital_equatorial_radius() * math.radians(fc.get_longitude() - start_longitude))
    ys.append(fc.get_altitude())

    # Draw x and y lists
    ax.clear()
    ax.plot(xs, ys)
    ax.grid(True)


# Set up plot to call animate() function periodically
ani = animation.FuncAnimation(fig, animate, fargs=(xs, ys), interval=1000)
plt.show()
