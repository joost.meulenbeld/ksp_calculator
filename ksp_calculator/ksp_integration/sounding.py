import math

import krpc
import time
import pandas
import matplotlib.pyplot as plt
import plotly.graph_objects as go
from plotly.subplots import make_subplots

time.sleep(1)
conn = krpc.connect("Connection")
vessel = conn.space_center.active_vessel
print(f"Connected to vesse {vessel.name}")
mean_altitude = conn.get_call(getattr, vessel.flight(), 'mean_altitude')

times = list()
density = list()
speed = list()
speed_of_sound = list()
drag = list()
altitude = list()

t_start = time.time()
# vessel.control.activate_next_stage()
vessel.control.throttle = 1

while vessel.resources.amount("SolidFuel") > 0.1:
    flight = vessel.flight(reference_frame=vessel.orbit.body.reference_frame)
    density.append(flight.atmosphere_density)
    speed.append(flight.speed)
    speed_of_sound.append(flight.speed_of_sound)
    drag.append(math.sqrt(sum(i ** 2 for i in flight.drag)))
    times.append(time.time() - t_start)
    altitude.append(flight.mean_altitude)

df = pandas.DataFrame(zip(times, density, speed, speed_of_sound, drag, altitude),
                      columns=["time", "density", "speed", "speed_of_sound", "drag", "altitude"])
df["q"] = 0.5 * df.density * (df.speed ** 2)
df["cdA"] = df.drag / df.q
df["mach"] = df.speed / df.speed_of_sound

fig, axes = plt.subplots(2, 4, figsize=(6, 6), sharex=True)
df.plot(x="time", ax=axes, subplots=True, grid=True)

df.plot.scatter(x="mach", y="cdA", grid=True)
df.plot.scatter(x="altitude", y="density", grid=True)
