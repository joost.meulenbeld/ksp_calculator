import math

import krpc
import time

time.sleep(1)
conn = krpc.connect("Connection")
vessel = conn.space_center.active_vessel
print(f"Connected to vesse {vessel.name}")
mean_altitude = conn.get_call(getattr, vessel.flight(), 'mean_altitude')

vessel.auto_pilot.target_pitch_and_heading(90, 90)
vessel.auto_pilot.engage()
vessel.control.throttle = 1

time.sleep(1)
print('Launch!')
vessel.control.activate_next_stage()


print("Waiting till fuel is empty")
fuel_amount = conn.get_call(vessel.resources.amount, 'SolidFuel')
expr = conn.krpc.Expression.less_than(
    conn.krpc.Expression.call(fuel_amount),
    conn.krpc.Expression.constant_float(0.1))
event = conn.krpc.add_event(expr)
with event.condition:
    event.wait()


print("Separating booster")
vessel.control.activate_next_stage()

# expr = conn.krpc.Expression.greater_than(
#     conn.krpc.Expression.call(mean_altitude),
#     conn.krpc.Expression.constant_double(1000)
# )
# event_ready_for_parachute = conn.krpc.add_event(expr)
print("Waiting till ready for parachute")


expr_altitude_higher_than_1000 = conn.krpc.Expression.less_than(
    conn.krpc.Expression.call(mean_altitude),
    conn.krpc.Expression.constant_double(1250)
)
event_ready_for_parachute = conn.krpc.add_event(expr_altitude_higher_than_1000)

with event_ready_for_parachute.condition:
    event_ready_for_parachute.wait()
print('Parachute!')

vessel.control.activate_next_stage()
