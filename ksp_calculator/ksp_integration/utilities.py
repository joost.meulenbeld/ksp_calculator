import time


def wait_for_next_iteration(space_center, sleep_time=0.0001):
    """
    Yield every time the simulation makes a new step.
    :param space_center: conn.space_center object
    :param sleep_time: Time to sleep waiting for next simulation step
    """
    prev_time = space_center.ut
    while True:
        while space_center.ut == prev_time:
            time.sleep(sleep_time)
        prev_time = space_center.ut
        yield
