import tkinter
from typing import List


class Option:
    def __init__(self, default, name="", callbacks=None):
        self.default = default
        self.name = name

        self.callbacks = callbacks
        if callbacks is None:
            self.callbacks = list()

        self.value = default

    @property
    def value(self):
        return self.__value

    @value.setter
    def value(self, value):
        self.__value = value
        for callback in self.callbacks:
            callback(value)

    def reset(self):
        self.value = self.default

    def control_buttons(self, frame: tkinter.Frame) -> List[tkinter.Button]:
        raise NotImplementedError


class Float(Option):
    def __init__(self, default=0., **kwargs):
        super().__init__(default, **kwargs)

    def multiply(self, fraction):
        self.value *= fraction

    def add(self, value):
        self.value += value

    def control_buttons(self, frame: tkinter.Frame) -> List[tkinter.Button]:
        values = [-1000, -100, -10, -1, -0.1, -0.01, -0.001, 0.001, 0.01, 0.1, 1, 10, 100, 1000]
        labels = [f"{'+' if v >= 0 else ''} {v}" for v in values]
        return [
            tkinter.Button(frame, text="* 1.2", command=lambda: self.multiply(1.2)),
            tkinter.Button(frame, text="/ 1.2", command=lambda: self.multiply(1 / 1.2)),
            *[tkinter.Button(frame, text=l, command=lambda v=v: self.add(v)) for l, v in zip(labels, values)]
        ]


class OptionList(Option):
    def __init__(self, option_list: List[str], default=None, **kwargs):
        self.option_list = option_list
        if default is None:
            default = option_list[0]
        super().__init__(default, **kwargs)
        assert default in option_list

    @Option.value.setter
    def value(self, value):
        assert value in self.option_list
        super(OptionList, self.__class__).value.fset(self, value)

    def control_buttons(self, frame: tkinter.Frame) -> List[tkinter.Button]:
        return [tkinter.Button(frame, text=v, command=lambda v=v: setattr(self, "value", v)) for v in self.option_list]


class ValueUpdaterApp:
    def __init__(self, master, options: List[Float]):
        self.options = options
        self.label_vars = [tkinter.StringVar() for _ in self.options]

        for label_var, option in zip(self.label_vars, self.options):
            label_var.set(option.value)
            option.callbacks.append(label_var.set)

        frame = tkinter.Frame(master)
        frame.pack()

        self.interface_rows = list()

        for i_row, f in enumerate(options):
            interface_row = [
                tkinter.Label(frame, text=f.name),
                tkinter.Label(frame, textvariable=self.label_vars[i_row]),
                tkinter.Button(frame, text="Reset", command=f.reset),
                *f.control_buttons(frame)
            ]

            for i_column, interface_column in enumerate(interface_row):
                interface_column.grid(row=i_row, column=i_column)

            self.interface_rows.append(interface_row)


class Displayable:
    def get_settings(self):
        return []


if __name__ == '__main__':
    root = tkinter.Tk()
    option_float = Float(4, name="fl", callbacks=[lambda x: print(f"Float {x}")])
    option_list = OptionList(["option1", "option2"], callbacks=[lambda x: print(f"Option {x}")])
    app = ValueUpdaterApp(root, [option_float, option_list])
    root.mainloop()
