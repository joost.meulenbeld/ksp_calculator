import math


def calculate_burn_time(max_thrust, specific_impulse, start_mass, delta_v):
    Isp = specific_impulse * 9.82
    m1 = start_mass / math.exp(delta_v / Isp)
    flow_rate = max_thrust / Isp
    burn_time = (start_mass - m1) / flow_rate
    return burn_time
