import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from ksp_calculator.physics.planet import get_kerbin


def plot_planet():
    kerbin = get_kerbin()
    altitude = np.linspace(-500, 75000, 1000)

    density = kerbin.atmosphere.density_from_altitude(altitude)
    gravity = kerbin.g_from_altitude(altitude)

    fig = make_subplots(specs=[[{"secondary_y": True}]])
    fig.add_trace(go.Scatter(x=altitude, y=density, name="Density [kg/m3]"))
    fig.add_trace(go.Scatter(x=altitude, y=gravity, name="Gravity [m/s2]"), secondary_y=True)
    fig.update_layout(title=go.layout.Title(text="Kerbin", xref="paper", x=0),
                      xaxis=go.layout.XAxis(title=go.layout.xaxis.Title(text="Altitude [m]")))
    fig.update_yaxes(title_text="Density [kg/m3]", secondary_y=False)
    fig.update_yaxes(title_text="Gravity [m/s2]", secondary_y=True)
    fig.show('browser')


if __name__ == '__main__':
    plot_planet()
