from typing import Union

import numpy as np

from ksp_calculator.physics.atmosphere import get_kerbin_atmosphere

GRAVITATIONAL_CONSTANT = 6.674e-11


class GravitationalBody:
    def __init__(self, mass: float, radius: float):
        self.mass = mass
        self.radius = radius

    def height_from_radius(self, radius: Union[float, np.ndarray]):
        return radius - self.radius

    def radius_from_height(self, height: Union[float, np.ndarray]):
        return height + self.radius

    def g_from_radius(self, radius: Union[float, np.ndarray]):
        return GRAVITATIONAL_CONSTANT * self.mass / (radius ** 2)

    def g_from_altitude(self, height: Union[float, np.ndarray]):
        return self.g_from_radius(self.radius_from_height(height))


class Planet(GravitationalBody):
    def __init__(self, mass: float, radius: float, atmosphere=None):
        super().__init__(mass=mass, radius=radius)
        self.atmosphere = atmosphere


def get_kerbin():
    return Planet(mass=5.2915158e22, radius=600e3, atmosphere=get_kerbin_atmosphere())


if __name__ == '__main__':
    kerbin = get_kerbin()
