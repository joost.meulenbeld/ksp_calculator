from typing import Union

import numpy as np


def return_np_or_float(func):
    def wrapper(arg: Union[float, np.ndarray]):
        return_type = type(arg)
        if not isinstance(arg, np.ndarray):
            arg = np.ndarray([arg])
        return_value = func(arg)
        return return_type(return_value)
    return wrapper


class Atmosphere:
    def __init__(self, max_height: float, sealevel_density: float, density_exponent: float,
                 pressure_scale_height: float = 5800, base_pressure: float = 101325, atmosphere_height: float=70000.):
        self.max_height = max_height
        self.sealevel_density = sealevel_density
        self.density_exponent = density_exponent
        self.pressure_scale_height = pressure_scale_height
        self.base_pressure = base_pressure
        self.atmosphere_height = atmosphere_height

    @return_np_or_float
    def density_from_altitude(self, altitude: Union[float, np.ndarray]):
        density = self.sealevel_density * np.exp(-altitude / self.density_exponent)
        density[altitude > self.atmosphere_height] = 0
        return density

    def pressure_from_altitude(self, altitude: Union[float, np.ndarray]):
        pressure = self.base_pressure * np.exp(altitude / self.pressure_scale_height)
        pressure[np.array(altitude) > self.atmosphere_height] = 0
        if isinstance(altitude, float):
            return float(pressure)
        return pressure


def get_kerbin_atmosphere():
    # From https://forum.kerbalspaceprogram.com/index.php?/topic/13368-air-density-on-kerbin/
    # However, the unit-tests fail since the values differ so much from those mentioned on the ksp wiki:
    # https://wiki.kerbalspaceprogram.com/wiki/Kerbin
    return Atmosphere(max_height=70e3, sealevel_density=1.223125, density_exponent=5000)
