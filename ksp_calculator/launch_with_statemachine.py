import tkinter
from threading import Thread
import asyncio
import janus

import krpc

from ksp_calculator import config
from ksp_calculator.interface.value_updater_app import ValueUpdaterApp
from ksp_calculator.ksp_integration.action_buttons import SaveGameLoader
from ksp_calculator.ksp_integration.flight_computer import FlightComputer
from ksp_calculator.state_machine.launch_state_machine import LaunchStateMachine
from ksp_calculator.state_machine.state_launch import LaunchStateEnum, LaunchEventEnum
from ksp_calculator.state_machine.state_machine_runner import LaunchStateMachineRunner
from ksp_calculator.state_machine.async_state_machine_runner import AsyncLaunchStateMachineRunner
from ksp_calculator.websocket_interface.socket_server import WebsocketInterface
from ksp_calculator.state_machine.ksp_streams import register_ksp_streams

conn = krpc.connect("Connection", address=config.ip)
fc = FlightComputer(conn)

state_machine = LaunchStateMachine(fc)

async def start_async():
    async_runner = AsyncLaunchStateMachineRunner(fc, state_machine)
    
    q = janus.Queue(100)
    register_ksp_streams(fc, q)

    socket_server = WebsocketInterface(
        fc, config.save_games,
        lambda force_state_name: state_machine.force_state(LaunchStateEnum[force_state_name]),
        lambda event_name: state_machine.run(LaunchEventEnum[event_name]))
    asyncio.ensure_future(socket_server.start(q.async_q))

    await async_runner.start(q.async_q)

if config.interface == "websocket":
    print("Starting Websocket interface")
    asyncio.run(start_async())


elif config.interface == "tk":
    print("Starting Tk interface")
    save_game_loader = SaveGameLoader(
        fc, config.save_games,
        lambda save_game_name: state_machine.force_state(LaunchStateEnum[config.save_games[save_game_name]])
    )

    runner = LaunchStateMachineRunner(fc, state_machine)
    thread_controller = Thread(target=runner.start)
    thread_controller.start()
    root = tkinter.Tk()
    app = ValueUpdaterApp(root, [*runner.get_settings(),
                                *save_game_loader.get_settings()])
    root.mainloop()

else:
    print("Non-existing interface")