import asyncio
import json
from json import JSONDecodeError
from typing import List

import websockets

from ksp_calculator.mission.event import get_event_by_name


class WebsocketInterface():
    def __init__(self, rx_queue: asyncio.Queue, tx_queue: asyncio.Queue, save_game_names: List[str]):
        self.rx_queue = rx_queue
        self.tx_queue = tx_queue
        self.save_game_names = save_game_names
        self.clients = []

    async def start(self):
        asyncio.ensure_future(websockets.serve(self.socket_listener, '0.0.0.0', 8765))
        asyncio.ensure_future(self.socket_sender())

    async def socket_sender(self):
        print('Waiting for events on the tx_queue')
        while event := await self.tx_queue.get():
            for client in self.clients:
                await client.send(json.dumps(event))

    async def socket_listener(self, websocket, path):
        print('New websocket client')
        self.clients.append(websocket)
        while True:
            try:
                message = await websocket.recv()
                await self.handle_websocket_message(websocket, message)

            except websockets.ConnectionClosed:
                print("Client disconnected")
                self.clients.remove(websocket)
                break

    async def handle_websocket_message(self, websocket, socket_data: str):
        try:
            decoded = json.loads(socket_data)

            message_type = decoded["message_type"]
            message = decoded.get("message", {})
            handler = {
                "event": self.handle_event_message,
                "get_save_games": self.handle_get_savegames,
            }.get(message_type)

            if handler is None:
                print(f"Can't handle message type {message_type}")
            else:
                await handler(websocket, message)
        except (JSONDecodeError, TypeError):
            print("Invalid json in websocket message")
        except KeyError:
            print(f"message_type not in available keys: {decoded.keys()}")
        except Exception as e:
            print(f"Some other exception: {e}")

    async def handle_event_message(self, _, message: dict):
        try:
            event_type = message["event_type"]
            event = get_event_by_name(event_type)
            payload = message.get("payload", {})
            self.rx_queue.put_nowait(event(**payload))
        except KeyError:
            print(f"event_type keys not in json keys: {message.keys()}")

    async def handle_get_savegames(self, websocket, _):
        message = {
            "message_type": "available_save_games",
            "message": {"save_games": self.save_game_names}
        }
        await websocket.send(json.dumps(message))

        # if "load_savegame" in json_msg:
        #     name = json_msg["load_savegame"]
        #     self.force_state_function("idle")
        #     self.fc.conn.space_center.load(json_msg["load_savegame"])
        #     self.force_state_function(self.save_game_names[name])
        # elif "event" in json_msg:
        #     self.async_q.put_nowait(json_msg["event"])
        # elif "force_state" in json_msg:
        #     self.force_state_function(json_msg["force_state"])
        # elif "get_savegames" in json_msg:
        #     message = { "name": "savegames", "data": self.save_game_names }
        #     await websocket.send(json.dumps(message))
