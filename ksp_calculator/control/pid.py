import numpy as np


class PID:
    def __init__(self, kp: float = 0, ki: float = 0, kd: float = 0, bounds=None, debug=False):
        self.kp = kp
        self.ki = ki
        self.kd = kd
        if isinstance(bounds, float) or isinstance(bounds, int):
            bounds = (-bounds, bounds)
        self.bounds = bounds
        self.debug = debug

        self.prev_time = 0
        self.setpoint = 0
        self.measurement = 0
        self.integrator = 0
        self.prev_error = None
        self.output = 0
        self.clipped_output = 0

    @property
    def error(self):
        return self.measurement - self.setpoint

    def reset_controller(self, measurement_time):
        self.prev_time = measurement_time
        self.measurement = 0
        self.integrator = 0
        self.prev_error = None
        self.output = 0
        self.clipped_output = 0

    def step(self, measurement, measurement_time, derivative=None):
        self.measurement = measurement
        dt = measurement_time - self.prev_time
        self.prev_time = measurement_time
        prev_integrator = self.integrator
        self.integrator += self.error * dt

        if derivative is None and self.prev_error is not None and dt != 0:
            derivative = (self.prev_error - self.error) / dt
        elif derivative is None:
            derivative = 0

        self.prev_error = self.error

        self.output = -(self.error * self.kp + self.integrator * self.ki + derivative * self.kd)
        if self.bounds is not None:
            self.clipped_output = clip(self.output, self.bounds[0], self.bounds[1])
            if self.clipped_output != self.output:
                self.integrator = prev_integrator
        else:
            self.clipped_output = self.output

        if self.debug:
            print(
                f"Setpoint: {self.setpoint:5.3f}, Value: {measurement:5.3f}, Output: {self.output:6.3f}, Integrator: {self.integrator:8.3f}, Derivative:{derivative:8.8f} Time: {self.prev_time:.3f}")
        return self.clipped_output


def clip(value, lower, upper):
    return float(np.clip(value, lower, upper))
