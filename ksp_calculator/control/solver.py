class SecantSolver:
    """This class iterates towards a zero-crossing of y in iterations of the secant method.
    Usage: successively call get_next_estimation to get a new x to sample at. Then call update_estimate with that new
    x and the sampled y.
    """

    def __init__(self, initial_guess=0.0, verbose=False):
        """
        :param x0: Initial guess for the zero-crossing
        :param verbose: If True, print out estimate and derivative info at each update_estimate call
        """
        self.initial_guess = initial_guess
        self.current_x = None
        self.current_y = None
        self.dydx_estimated = None
        self.verbose = verbose

    def get_estimation_point(self):
        """Get the next x to sample at."""
        if self.current_x is None:
            return self.initial_guess
        if self.current_y is None:
            return self.current_x
        if self.dydx_estimated is None:
            return self.current_x + 1
        return self.current_x - self.current_y / self.dydx_estimated

    def update_estimate(self, new_x, new_y):
        """Supply a new sample to the solver."""
        assert new_x != self.current_x
        if self.current_x is not None and self.current_x is not None:
            self.dydx_estimated = (new_y - self.current_y) / (new_x - self.current_x)
            if self.verbose:
                print(f"New derivative estimate ({new_y} - {self.current_y}) / ({new_x} - {self.current_x}) "
                      f"= {self.dydx_estimated}")
        self.current_x = new_x
        self.current_y = new_y


if __name__ == '__main__':
    f = lambda x: (x - 5) ** 2 - 4  # Root at +3 and +7, solver should find +3 because default guess is 0

    solver = SecantSolver()
    for _ in range(10):
        sample_point = solver.get_estimation_point()
        new_sample = f(sample_point)
        solver.update_estimate(sample_point, new_sample)
        print(sample_point, new_sample)
