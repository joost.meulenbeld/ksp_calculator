import sympy

a_thrust, g, h_dot_0, h_0, t_coast, h_ref, h_dot_ref = sympy.symbols("a_thrust, g, h_dot_0, h_0, t_coast, h_ref, h_dot_ref", positive=True)
t_burn = sympy.symbols("t_burn")


h_dot_burn = h_dot_0 + (a_thrust - g) * t_burn
h_burn = sympy.integrate(h_dot_burn, t_burn) + h_0

h_dot_coast = (h_dot_burn - g * t_coast).simplify()

h_coast = (sympy.integrate(h_dot_coast, t_coast) + h_burn).simplify()

t_coast_sol = sympy.solve(h_dot_coast - h_dot_ref, t_coast)[0]
h_coast = h_coast.subs(t_coast, t_coast_sol).simplify()

t_burn_sol = list(sympy.solveset(h_coast - h_ref, t_burn))

print(f"Burn-then-coast: {t_burn_sol}")
