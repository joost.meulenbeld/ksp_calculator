import math
from typing import Dict

import numpy as np

from ksp_calculator.control.pid import PID
from ksp_calculator.interface.value_updater_app import Float, OptionList, Displayable
from ksp_calculator.ksp_integration.flight_computer import FlightComputer
from ksp_calculator.ksp_integration.locations import LocationManager
from ksp_calculator.ksp_integration.utilities import wait_for_next_iteration


def get_thrust_from_vertical_acceleration_without_aero(g, mass, vertical_direction, sp_acceleration):
    thrust_acceleration = sp_acceleration + g
    thrust = thrust_acceleration * mass / vertical_direction
    return thrust

def get_thrust_from_vertical_acceleration(fc: FlightComputer, sp_acceleration):
    """Get the required thrust (Newton) to attain sp_acceleration, taking aerodynamics and pitch angle into account"""
    mass = fc.get_mass()
    vertical_base_acceleration = fc.velocity_ref_flight.aerodynamic_force[0] / mass - fc.get_g()
    thrust_acceleration = sp_acceleration - vertical_base_acceleration
    thrust = thrust_acceleration * mass / fc.get_vertical_direction()
    return thrust


def get_remaining_burn_time(h_0, h_dot_0, h_ref, h_dot_ref, g, a_thrust):
    det = -2 * a_thrust * g * h_0 + 2 * a_thrust * g * h_ref + a_thrust * h_dot_ref ** 2 + \
          2 * g ** 2 * h_0 - 2 * g ** 2 * h_ref + g * h_dot_0 ** 2 - g * h_dot_ref ** 2
    if det >= 0 and a_thrust > 0:
        t_burn_remaining = h_dot_0 / (-a_thrust + g) + math.sqrt(det) / (math.sqrt(a_thrust) * (a_thrust - g))
        return t_burn_remaining
    elif det < 0:
        print("Determinant was larger than 0")
    elif a_thrust <= 0:
        print("Thrust acceleration is not in right direction")
    return 0


def should_burn(h_0, h_dot_0, h_ref, h_dot_ref, g, a_thrust):
    """Return True if the rocket should thrust fully"""
    det = -2 * a_thrust * g * h_0 + 2 * a_thrust * g * h_ref + a_thrust * h_dot_ref ** 2 + \
          2 * g ** 2 * h_0 - 2 * g ** 2 * h_ref + g * h_dot_0 ** 2 - g * h_dot_ref ** 2
    if det >= 0 and a_thrust > 0:
        t_burn_remaining = h_dot_0 / (-a_thrust + g) + math.sqrt(det) / (math.sqrt(a_thrust) * (a_thrust - g))
        return t_burn_remaining > 3 * 1 / 50  # Empirical 5 frames delay between command and thrust
    return False


_sp_alt = Float(name="Setpoint altitude")
_kp_vspeed = Float(name="Kp vspeed")


class Controller(Displayable):
    def __init__(self, fc: FlightComputer):
        self.fc = fc
        self.stop_flag = True
        self.running = False

    def step(self):
        raise NotImplementedError

    def start(self):
        """Call this function to start the controller"""
        self.stop_flag = False
        self.startup()
        self.run()
        self.cleanup()

    def run(self):
        self.running = True
        for _ in wait_for_next_iteration(self.fc.conn.space_center):
            if self.stop_flag:
                break
            self.step()
        self.running = False
        self.stop_flag = False

    def stop(self):
        self.stop_flag = True

    def startup(self):
        pass

    def cleanup(self):
        pass


class HorizontalPositionController(Controller):
    def __init__(self, *args, max_angle=60, kp_hspeed=8400, kd_hspeed=-36000, north_setpoint=-0.0971901709300802,
                 east_setpoint=-74.55768367523336):
        super().__init__(*args)

        self.north_controller = PID(debug=False)
        self.east_controller = PID(debug=False)

        self.north_setpoint = Float(north_setpoint, name="SP North",
                                    callbacks=[lambda v: setattr(self.north_controller, "setpoint", v)])
        self.east_setpoint = Float(east_setpoint, name="SP East",
                                   callbacks=[lambda v: setattr(self.east_controller, "setpoint", v)])

        def preset_setpoint(value, north_setpoint, east_setpoint, fc):
            if value == "Here":
                north_setpoint.value = fc.velocity_ref_flight.latitude
                east_setpoint.value = fc.velocity_ref_flight.longitude
            else:
                coordinate = LocationManager[value].coord
                north_setpoint.value = coordinate.lat
                east_setpoint.value = coordinate.lon

        self.preset_setpoint = OptionList(["Here", *LocationManager.keys()], name="Horizontal preset",
                                          callbacks=[
                                              lambda value: preset_setpoint(value, self.north_setpoint,
                                                                            self.east_setpoint, self.fc)])

        self.kp_hspeed = Float(kp_hspeed, name="Kp hspeed",
                               callbacks=[lambda v: setattr(self.north_controller, "kp", v),
                                          lambda v: setattr(self.east_controller, "kp", v)])
        self.kd_hspeed = Float(kd_hspeed, name="Kd hspeed",
                               callbacks=[lambda v: setattr(self.north_controller, "kd", v),
                                          lambda v: setattr(self.east_controller, "kd", v)])

        self.max_angle = Float(max_angle, name="Max angle")

    def startup(self):
        self.north_controller.reset_controller(self.fc.conn.space_center.ut)
        self.east_controller.reset_controller(self.fc.conn.space_center.ut)
        self.fc.engage_autopilot()

    def step(self):
        ut = self.fc.conn.space_center.ut
        north_angle = self.north_controller.step(self.fc.velocity_ref_flight.latitude, ut)
        east_angle = self.east_controller.step(self.fc.velocity_ref_flight.longitude, ut)

        total_angle = math.sqrt(north_angle ** 2 + east_angle ** 2)
        rescale = max(1., total_angle / self.max_angle.value)
        north_angle /= rescale
        east_angle /= rescale
        total_angle /= rescale

        sp_pitch = 90 - total_angle
        sp_heading = math.degrees(math.atan2(east_angle, north_angle))
        # print(f"East {east_angle} North {north_angle}, Pitch {sp_pitch}, Heading {sp_heading}")

        self.fc.vessel.auto_pilot.target_heading = sp_heading
        self.fc.vessel.auto_pilot.target_pitch = sp_pitch

    def cleanup(self):
        self.fc.disengage_autopilot()

    def get_settings(self):
        return [self.kp_hspeed, self.kd_hspeed, self.north_setpoint, self.east_setpoint, self.preset_setpoint,
                self.max_angle]


class VerticalSpeedController(Controller):
    def __init__(self, *args, kp=10):
        super().__init__(*args)
        self.vspeed_controller = PID(kp, debug=False)
        self.sp_vspeed = Float(name="Setpoint Vspeed",
                               callbacks=[lambda v: setattr(self.vspeed_controller, "setpoint", v)])

        self.kp_vspeed = _kp_vspeed
        self.kp_vspeed.callbacks.append(lambda v: setattr(self.vspeed_controller, "kp", v))
        self.kp_vspeed.value = kp

    def step(self):
        sp_acceleration = self.vspeed_controller.step(self.fc.get_surface_velocity()[0], self.fc.conn.space_center.ut)
        thrust = get_thrust_from_vertical_acceleration(self.fc, sp_acceleration)
        if self.fc.vessel.available_thrust == 0:
            throttle = 0
        else:
            throttle = np.clip(thrust / self.fc.vessel.available_thrust, 0, 1)

        self.fc.set_throttle(throttle)

    def get_settings(self):
        return [self.sp_vspeed, self.kp_vspeed]

    def startup(self):
        self.vspeed_controller.reset_controller(self.fc.conn.space_center.ut)


class LandingController(Controller):
    def __init__(self, *args, kp=10):
        super().__init__(*args)
        self.vspeed_controller = PID(kp, debug=False)
        self.kp_vspeed = _kp_vspeed
        self.kp_vspeed.callbacks.append(lambda v: setattr(self.vspeed_controller, "kp", v))
        self.kp_vspeed.value = kp
        self.gear_activated = self.fc.vessel.control.gear

    def step(self):
        height = self.fc.get_height()
        self.vspeed_controller.setpoint = min(-2., -height / 3)
        if height < 30 and not self.gear_activated:
            self.gear_activated = True
            self.fc.vessel.control.gear = True

        sp_acceleration = self.vspeed_controller.step(self.fc.get_surface_velocity()[0], self.fc.conn.space_center.ut)
        thrust = get_thrust_from_vertical_acceleration(self.fc, sp_acceleration)
        available_thrust = self.fc.vessel.available_thrust
        if available_thrust == 0:
            throttle = 0
        else:
            self.unclipped_throttle = thrust / available_thrust
            throttle = np.clip(self.unclipped_throttle, 0, 1)

        self.fc.set_throttle(throttle)

    def get_settings(self):
        return [self.kp_vspeed]

    def startup(self):
        self.vspeed_controller.reset_controller(self.fc.conn.space_center.ut)

    def cleanup(self):
        self.fc.vessel.control.gear = False


class VerticalNestedController(Controller):
    def __init__(self, *args, kp_alt=0.7, kp_vspeed=10, sp_alt=200, vspeed_bounds=(-100, 100)):
        super().__init__(*args)
        self.altitude_controller = PID(debug=False, bounds=vspeed_bounds)
        self.vspeed_controller = PID(debug=False)

        self.kp_alt = Float(kp_alt, name="Kp altitude",
                            callbacks=[lambda v: setattr(self.altitude_controller, "kp", v)])
        self.kp_vspeed = _kp_vspeed
        self.kp_vspeed.callbacks.append(lambda v: setattr(self.vspeed_controller, "kp", v))
        self.kp_vspeed.value = kp_vspeed

        self.sp_alt = _sp_alt
        self.sp_alt.value = sp_alt
        self.sp_alt.callbacks.append(lambda v: setattr(self.altitude_controller, "setpoint", v))

    def step(self):
        """Vertical position control loop with nested P controllers"""
        sp_vspeed = self.altitude_controller.step(self.fc.get_altitude(), self.fc.conn.space_center.ut,
                                                  derivative=self.fc.get_surface_velocity()[0])

        self.vspeed_controller.setpoint = sp_vspeed
        sp_acceleration = self.vspeed_controller.step(self.fc.get_surface_velocity()[0], self.fc.conn.space_center.ut)

        thrust = get_thrust_from_vertical_acceleration(self.fc, sp_acceleration)
        if self.fc.vessel.available_thrust == 0:
            throttle = 0
        else:
            throttle = np.clip(thrust / self.fc.vessel.available_thrust, 0, 1)

        self.fc.vessel.control.throttle = throttle
        # if np.linalg.norm((north_controller.error, east_controller.error)) <= 0.0005:
        #     sp_alt.value = 170
        # else:
        #     sp_alt.value = 180
        # app.update_values()

    def get_settings(self):
        return [self.kp_alt, self.kp_vspeed, self.sp_alt]

    def startup(self):
        self.vspeed_controller.reset_controller(self.fc.conn.space_center.ut)
        self.altitude_controller.reset_controller(self.fc.conn.space_center.ut)


class VerticalMaxThrustController(Controller):
    def __init__(self, *args, sp_alt=200, debug=True):
        super().__init__(*args)
        self.sp_alt = _sp_alt
        self.sp_alt.value = sp_alt
        self.debug = debug

    def step(self):
        vertical_base_acceleration = self.fc.velocity_ref_flight.aerodynamic_force[0] / self.fc.get_mass() \
                                     - self.fc.get_g()
        burn = should_burn(self.fc.get_altitude(), self.fc.velocity_ref_flight.velocity[0], self.sp_alt.value,
                           0,
                           -vertical_base_acceleration,
                           self.fc.vessel.max_thrust / self.fc.get_mass() * self.fc.get_vertical_direction())

        if self.debug:
            print(
                f"Alt {self.fc.get_altitude()}, sp {self.sp_alt.value}, base acc {vertical_base_acceleration}, burn {burn}")
        self.fc.vessel.control.throttle = float(burn)

    def get_settings(self):
        return [self.sp_alt]


class LinearSimpleLaunchController(Controller):
    def __init__(self, pitch_profile, *args, target_epiapsis_altitude=80e3, target_apoapsis_altitude=80e3, debug=True):
        super().__init__(*args)
        self.pitch_profile = pitch_profile
        self.target_epiapsis_altitude = target_epiapsis_altitude
        self.target_apoapsis_altitude = target_apoapsis_altitude
        self.debug = debug

    def step(self):
        sp_pitch = np.interp(self.fc.get_altitude(), self.pitch_profile[0], self.pitch_profile[1], left=90, right=0)

        if self.fc.get_periapsis_altitude() >= self.target_epiapsis_altitude:
            throttle = 0
        elif self.fc.get_apoapsis_altitude() < self.target_apoapsis_altitude:
            throttle = 1
        elif self.fc.get_time_to_apoapsis() < 20:
            throttle = 1
        else:
            throttle = 0

        if self.debug:
            print(f"Pitch {sp_pitch}, throttle {throttle}")

        self.fc.set_pitch(sp_pitch)
        self.fc.set_heading(90)
        self.fc.set_throttle(throttle)


class ControllerSwitcher(Controller):
    def __init__(self, fc: FlightComputer, controllers: Dict[str, Controller], start_controller_name: str,
                 name="Control mode"):
        super().__init__(fc)
        self.controllers = controllers
        self.current_controller_name = start_controller_name
        self.new_controller_name = start_controller_name
        self.controller_picker = OptionList(list(controllers.keys()), name=name, callbacks=[lambda v: self.switch(v)])

    @property
    def current_controller(self):
        return self.controllers[self.current_controller_name]

    def switch(self, controller_name):
        self.new_controller_name = controller_name

    def step(self):
        if self.current_controller_name != self.new_controller_name:
            self.current_controller.running = False
            self.current_controller.stop_flag = False
            self.current_controller.cleanup()

            self.current_controller_name = self.new_controller_name

            self.current_controller.startup()
            self.current_controller.running = True
            self.current_controller.stop_flag = False

        self.current_controller.step()

    def get_settings(self):
        settings = [self.controller_picker]
        for controller in self.controllers.values():
            for setting in controller.get_settings():
                if setting not in settings:
                    settings.append(setting)
        return settings

    def startup(self):
        self.fc.engage_autopilot()

    def cleanup(self):
        self.fc.disengage_autopilot()
