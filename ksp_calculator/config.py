from typing import Dict

import yaml

with open("config.yml") as f:
    config = yaml.full_load(f)

ip = config["ip"]
save_games = config["save_games"]
interface = config["interface"] if "interface" in config else "tk"

assert isinstance(ip, str), "config.ip should be a string"
assert isinstance(save_games, dict), "config.save_games should be dict mapping save_game_name -> state"
assert (interface in ["tk", "websocket"]), "config.interface should be a either tk or websocket"