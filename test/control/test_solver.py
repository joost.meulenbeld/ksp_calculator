from ksp_calculator.control.solver import SecantSolver


class TestSecantSolver:
    def test_initial_guess_is_zero(self):
        # Arrange
        sut = SecantSolver()

        # Act
        sample_point = sut.get_estimation_point()

        # Assert
        assert sample_point == 0

    def test_initial_guess_is_set_value(self):
        # Arrange
        sut = SecantSolver(5)

        # Act
        sample_point = sut.get_estimation_point()

        # Assert
        assert sample_point == 5

    def test_first_derivative_estimate(self):
        # Arrange
        sut = SecantSolver()
        sut.update_estimate(6, 10)

        # Act
        sample_point = sut.get_estimation_point()

        # Assert
        assert sample_point == 7  # First estimate point plus 1

    def test_sample_point_is_calculated_with_derivative(self):
        # Arrange
        sut = SecantSolver()
        sut.update_estimate(10, 10)
        sut.update_estimate(11, 12)

        # Act
        sample_point = sut.get_estimation_point()

        # Assert
        assert sample_point == 5
