from ksp_calculator.control.pid import clip


def test_clip():
    assert -1 == clip(-2, -1, 0)
    assert 0 == clip(0, -1, 0)
    assert 0 == clip(1, -1, 0)
