from typing import NamedTuple

class Vehicle(NamedTuple):
    name: str
    dry_mass: float # [kg]
    max_thrust: float # [N]
    max_massflow: float # [kg/s]