import numpy as np

class Body:
    def __init__(self, name, mu, radius, sidereal_rotation_period=0):
        self.name = name
        self.mu = mu
        self.radius = radius
        self.sidereal_rotation_period = sidereal_rotation_period

    def __str__(self):
        return f"Body:\t{self.name}\n" \
            f"mu:\t{self.mu}\n" \
            f"radius:\t{self.radius} [m]\n" \
            f"rot:\t{np.rad2deg(self.get_angular_velocity()):.5f} [deg/s]\n"

    def get_circular_orbit_velocity(self, radius):
        return np.sqrt(self.mu / radius)

    def get_angular_velocity(self):
        return (2 * np.pi) / self.sidereal_rotation_period

class Mun(Body):
    def __init__(self):
        super().__init__(
            name='Mun',
            mu=6.5138398e10,
            radius=200_000,
            sidereal_rotation_period=138_984.38
        )


if __name__ == '__main__':
    mun = Mun()
    print(mun)