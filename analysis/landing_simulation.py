import math

import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import solve_ivp


def controller(t, x):
    if t > 140:
        return np.array([1, math.radians(-30)])
    if t > 50:
        return np.array([0.4, math.radians(-80)])
    return np.array([0, 0])


def get_f_constructor(Tmax, g, mdot_max, m_dry):
    def construct_f(throttle, theta):
        def x_dot(_, x_):
            xdot = x_[1]
            zdot = x_[3]
            m = x_[4] + m_dry
            Fx = math.sin(theta) * throttle * Tmax
            Fz = math.cos(theta) * throttle * Tmax
            return np.array([
                xdot,
                Fx / m,
                zdot,
                Fz / m - g,
                - throttle * mdot_max
            ])

        return x_dot

    return construct_f


def simulate_controller():
    # Situation
    Tmax = 60_000  # N
    mdot = 17.73  # kg/s
    g = 1.63  # m/s2
    m_dry = 2.08e3
    construct_f = get_f_constructor(Tmax, g, mdot, m_dry)

    # Initial state
    v0 = 543  # m/s
    gamma0 = math.radians(0)
    m_prop0 = 3.52e3 - m_dry
    h0 = 10_000

    # Simulation parameters
    dt = 1
    t_max = 200
    t_values = np.arange(0, t_max, dt)
    x_array = np.zeros((5, len(t_values)))
    x_array[:, 0] = np.array([
        0,
        v0 * math.cos(gamma0),
        h0,
        - v0 * math.sin(gamma0),
        m_prop0,
    ])
    u_array = np.zeros((2, len(t_values)))

    # Run simulation
    for i in range(len(t_values) - 1):
        u_array[:, i] = controller(t_values[i], x_array[:, i])
        f = construct_f(u_array[0, i], u_array[1, i])
        x_array[:, i + 1] = solve_ivp(f, [t_values[i], t_values[i + 1]], x_array[:, i],
                                      t_eval=[t_values[i + 1]]).y.flatten()
        if x_array[2, i + 1] < 0:
            break

    u_array[:, -1] = controller(t_values[-1], x_array[:, -1])
    return t_values[:i + 1], x_array[:, :i + 1], u_array[:, :i + 1]


if __name__ == '__main__':
    t, x, u = simulate_controller()

    labels = ["x", "xdot", "z", "zdot", "mfuel"]
    fig, axes = plt.subplots(3, 2)
    for ax, data, label in zip(axes.flatten(), x, labels):
        ax.plot(t, data)
        ax.set_ylabel(label)
        ax.set_xlabel("t")
        ax.grid()

    axes.flatten()[-1].plot(x[0, :], x[2, :])
    axes.flatten()[-1].set_xlabel("x")
    axes.flatten()[-1].set_ylabel("z")
    axes.flatten()[-1].grid()
    plt.show()
