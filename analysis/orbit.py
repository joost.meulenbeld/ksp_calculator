import numpy as np
from analysis.bodies import Mun

class Orbit:

    def __init__(self, body, a, e):
        self.body = body        
        self.a = a          # Semi-major axis in [m]
        self.e = e          # Eccentricity [-]
        self.E = None       # Eccentric anomaly [rad]
        self.M = None       # Mean anomaly [rad]
        self.nu = None      # True anomaly [rad]

        # Calculated variables
        self.n = np.sqrt(self.body.mu / self.a**3) # Mean angular motion [rad/s]
        self.apoapsis = self.a * (1 + self.e) # Apoapsis [m]
        self.periapsis = self.a * (1 - self.e) # Periapsis [m]

    def set_position_from_radius(self, r, sign):
        cosE = (1 / self.e) - r / (self.a * self.e)
        self.E = sign * np.arccos(cosE)
        self.M = self.E - self.e * np.sin(self.E)
        self.nu = np.arccos((cosE - self.e) / (1 - self.e * cosE))

    def time_to_apoapsis(self):
        return (np.pi - self.M) / self.n

    def velocity_at_apoapsis(self):
        return np.sqrt(self.body.mu * (2 / self.apoapsis - 1 / self.a))

    def velocity_at_periapsis(self):
        return np.sqrt(self.body.mu * (2 / self.periapsis - 1 / self.a))

    def __str__(self):
        return f"Body:\t{self.body.name}\n" \
            f"a:\t\t{self.a:.1f} [m]\n" \
            f"e:\t\t{self.e:.5f} [-]\n" \
            f"Apoapsis:\t{self.apoapsis:.1f} [m]\n" \
            f"Periapsis:\t{self.periapsis:.1f} [m]\n" \
            f"n:\t\t{np.rad2deg(self.n):.3f} [deg/s]\n" \
            f"Position:\n"\
            f"E:\t\t{np.rad2deg(self.E):.1f} [deg]\n" \
            f"M:\t\t{np.rad2deg(self.M):.1f} [deg]\n" \
            f"nu:\t\t{np.rad2deg(self.nu):.1f} [deg]\n" \
            f"Timings:\n" \
            f"Time to apoapsis: {self.time_to_apoapsis():.1f} [s]\n" \
            f"Velocities:\n" \
            f"At apoapsis:\t{self.velocity_at_apoapsis():.1f} [m/s]\n" \
            f"At periapsis:\t{self.velocity_at_periapsis():.1f} [m/s]\n" \

    @staticmethod
    def from_polar_state(body, r, rdot, thetadot):
        vtheta = thetadot * r
        V = np.sqrt(rdot ** 2 + vtheta ** 2)
        gamma = np.arctan2(vtheta, rdot)
        sign = np.sign(gamma)
        a = 1 / (2 / r - V ** 2 / body.mu)
        e = np.sqrt(1 - (r ** 2 * V ** 2 * np.sin(gamma) ** 2 / (body.mu * a)))

        orbit = Orbit(body, a, e)
        orbit.set_position_from_radius(r, sign)
        return orbit


if __name__ == '__main__':
    # This is just after takeoff when apoaps is expected to be at approx 210k
    body = Mun()
    r = 203_370
    rdot = 99.52
    thetadot = 0.0020036
    
    orbit = Orbit.from_polar_state(body, r, rdot, thetadot)
    print(orbit)