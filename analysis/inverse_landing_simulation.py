import math
from typing import NamedTuple, Optional, Tuple

import matplotlib.pyplot as plt
import numpy as np
import pandas
from scipy.integrate import solve_ivp
from tqdm import tqdm

from analysis.vehicle import Vehicle
from analysis.bodies import Mun
from analysis.orbit import Orbit

controller_state = None


def controller(vehicle, body, t, x):
    global controller_state

    target_altitude = 10_000
    landing_pitch = 10
    deorbit_throttle = 0.5
    target_radius = body.radius + target_altitude

    current_velocity = np.sqrt(x[1] ** 2 + (x[0] * x[3]) ** 2)
    orbit = Orbit.from_polar_state(body, x[0], x[1], x[3])
    required_orbital_velocity = body.get_circular_orbit_velocity(target_radius)

    if controller_state == "takeoff_burn":
        if orbit.apoapsis >= target_radius:
            controller_state = "coasting"
            return np.array([0, 0])
        error = target_radius - orbit.apoapsis
        throttle = min(0.9, 0.001 * error)
        return np.array([throttle, math.radians(landing_pitch)])

    elif controller_state == "coasting":
        # continuously calculating the same thing now
        delta_v = required_orbital_velocity - orbit.velocity_at_apoapsis()
        # assume constant mass :o and add dry mass
        approx_burn_time = delta_v / (vehicle.max_thrust * deorbit_throttle / (vehicle.dry_mass + x[4]))
        if orbit.time_to_apoapsis() < (approx_burn_time / 2):
            controller_state = "circularizing"
        return np.array([0, 0])

    elif controller_state == "circularizing":
        if current_velocity >= required_orbital_velocity:
            controller_state = "circularized"
            return np.array([0, 0])
        error = required_orbital_velocity - current_velocity
        throttle = min(deorbit_throttle, 0.2 * error)
        return np.array([throttle, 0])

    elif controller_state == "circularized":
        return np.array([0, 0])

    else:
        raise ValueError(f"Controller state {controller_state} is unkown")
    # elif(min(predicted_apoapsis, predicted_periapsis) < 209_000):
    #     return np.array([1, math.radians(0)])
    # return np.array([0, 0])


def get_f_constructor(vehicle, body):
    def construct_f(throttle, pitch):
        def x_dot(_, x_):
            g = body.mu / x_[0] ** 2
            mass = vehicle.dry_mass + x_[4]
            thrust_acceleration = throttle * vehicle.max_thrust / mass
            return np.array([
                x_[1],
                -g + x_[0] * x_[3] ** 2 + thrust_acceleration * math.sin(pitch),
                x_[3],
                -2 * x_[1] * x_[3] / x_[0] + thrust_acceleration * math.cos(pitch) / x_[0],
                throttle * vehicle.max_massflow
            ])

        return x_dot

    return construct_f



def simulate_controller(vehicle, body, landed_fuel):
    global controller_state
    controller_state = "takeoff_burn"
    construct_f = get_f_constructor(vehicle, body)

    # Simulation parameters
    dt = 0.1
    t_max = 400
    t_values = np.arange(0, t_max, dt)
    x_array = np.zeros((5, len(t_values)))
    u_array = np.zeros((2, len(t_values)))

    # Initial state        print(error, throttle)
    x_array[:, 0] = np.array([
        202_814,
        0,
        0,
        body.get_angular_velocity(),  # due to angular velocity of the mun itself
        landed_fuel
    ])

    # Run simulation
    for i in tqdm(range(len(t_values) - 1), desc="Running simulation"):
        u_array[:, i] = controller(vehicle, body, t_values[i], x_array[:, i])
        f = construct_f(u_array[0, i], u_array[1, i])
        x_array[:, i + 1] = solve_ivp(f, [t_values[i], t_values[i + 1]], x_array[:, i],
                                    t_eval=[t_values[i + 1]]).y.flatten()

    u_array[:, -1] = controller(vehicle, body, t_values[-1], x_array[:, -1])

    return t_values[:i + 1], x_array[:, :i + 1], u_array[:, :i + 1]


def calculate_mission_profile():
    vehicle = Vehicle(
        name = 'Mun lander',
        max_thrust = 60_000,  # N
        max_massflow = 17.73,  # kg/s
        dry_mass = 2082.49951171875,
    )
    body = Mun()

    orbit_fuel = 3548.375732421875 - vehicle.dry_mass
    landed_fuel = 913.4
    for i in range(0, 10):
        t, x, u = simulate_controller(vehicle, body, landed_fuel)
        print(f"{i}. orbit fuel: {x[4, -1]:.1f} should be: {orbit_fuel:.1f}. -> landed fuel: {landed_fuel}")
        diff = orbit_fuel - x[4, -1]
        if (np.abs(diff) < 1.0):
            break
        landed_fuel += diff

    return t, x, u

if __name__ == '__main__':
    vehicle = Vehicle(
        name = 'Mun lander',
        max_thrust = 60_000,  # N
        max_massflow = 17.73,  # kg/s
        dry_mass = 2082.49951171875,
    )
    body = Mun()

    orbit_fuel = 3548.375732421875 - vehicle.dry_mass
    landed_fuel = 913.4
    for i in range(0, 10):
        t, x, u = simulate_controller(vehicle, body, landed_fuel)
        print(f"{i}. orbit fuel: {x[4, -1]:.1f} should be: {orbit_fuel:.1f}. -> landed fuel: {landed_fuel}")
        diff = orbit_fuel - x[4, -1]
        if (np.abs(diff) < 1.0):
            break
        landed_fuel += diff

    x_u = np.concatenate((x, u))

    df = pandas.DataFrame(x_u.T, columns=["r", "rdot", "theta", "thetadot", "mfuel", "throttle", "pitch"])
    df["t"] = t
    df["x"] = df.r * np.cos(df.theta)
    df["z"] = df.r * np.sin(df.theta)
    df["theta_deg"] = np.rad2deg(df.theta)
    df["pitch_deg"] = np.rad2deg(df.pitch)
    df["h"] = df.r - body.radius
    df["vtheta"] = df.thetadot * df.r
    df["V"] = np.sqrt(df.rdot ** 2 + df.vtheta ** 2)

    df["gamma"] = np.arctan2(df.vtheta, df.rdot)
    df["gamma_deg"] = np.rad2deg(df.gamma)

    orbits = Orbit.from_polar_state(body, df.r, df.rdot, df.thetadot)
    df["a"] = orbits.a
    df["e"] = orbits.e
    df["apoapsis"] = orbits.apoapsis
    df["periapsis"] = orbits.periapsis
    df["eccentric_anomaly"] = orbits.E
    df["mean_anomaly"] = orbits.M
    df["true_anomaly"] = orbits.nu
    df["mean_motion"] = orbits.n

    df["v_at_apoapsis"] = orbits.velocity_at_periapsis()
    df["v_at_periapsis"] = orbits.velocity_at_periapsis()
    df["time_to_apoapsis"] = orbits.time_to_apoapsis()

    df["longitude"] = np.deg2rad(-10) - df.theta + df.t * body.get_angular_velocity()
    df["longitude_deg"] = np.rad2deg(df.longitude)

    # print(df.iloc[-1])


    class PlotOptions(NamedTuple):
        x_label: str
        y_label: str
        grid_equal: bool = False
        polar: bool = False
        x_limits: Optional[Tuple[float, float]] = None
        y_limits: Optional[Tuple[float, float]] = None


    plot_labels = [
        PlotOptions("t", "h"),
        PlotOptions("t", "V"),
        PlotOptions("t", "mfuel"),

        PlotOptions("z", "x", grid_equal=True, x_limits=(body.radius * 1.1, -body.radius * 1.1),
                    y_limits=(-body.radius * 1.1, body.radius * 1.1)),
        # PlotOptions("z", "x", grid_equal=True,
        #             x_limits=(df.z.max() + 1e3, df.z.min() - 1e3)),
        PlotOptions("t", "throttle"),

        PlotOptions("t", "pitch_deg"),
        PlotOptions("t", "apoapsis"),
        PlotOptions("t", "periapsis"),

        # PlotOptions("t", "v_at_apoapsis")
        PlotOptions("t", "longitude_deg")

    ]

    fig = plt.figure()
    plot_size = (3, 3)
    for i, plot_options in enumerate(plot_labels):
        projection = "polar" if plot_options.polar else "rectilinear"
        ax = fig.add_subplot(*plot_size, i + 1, projection=projection)
        ax.plot(df[plot_options.x_label], df[plot_options.y_label])
        ax.set_xlabel(plot_options.x_label)
        ax.set_ylabel(plot_options.y_label)
        if not plot_options.polar:  # For polar projection, setting grid actually disables it
            ax.grid()
        if plot_options.grid_equal:
            ax.set_aspect('equal', adjustable='box')
        if plot_options.x_limits:
            ax.set_xlim(*plot_options.x_limits)
        if plot_options.y_limits:
            ax.set_ylim(*plot_options.y_limits)

    plt.show()
